package be.johanbas.y2018.day02;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day02 {

    public static void main(String[] args) throws IOException, URISyntaxException {

//        part1();
        part2();

    }

    private static void part2() throws URISyntaxException, IOException {
        List<String> strings = readInput();
        for (int i = 0; i < strings.size(); i++) {
            for (int j = i; j < strings.size(); j++) {
                if(oneOff(strings.get(i).toCharArray(), strings.get(j).toCharArray())) {
                    System.out.println("j = " + strings.get(j));
                    System.out.println("i = " + strings.get(i));
                }
            }
        }
    }

    private static boolean oneOff(char[] a, char[] b) {
        int diff = 0;
        for (int i = 0; i < a.length; i++) {
            if(a[i] != b[i]) {
                diff += 1;
            }
        }

        return diff == 1;
    }

    private static void part1() throws URISyntaxException, IOException {
        int count2 = 0;
        int count3 = 0;

        List<String> strings = readInput();
        for (String string : strings) {
            Map<Character, Integer> map = createMap(string);
            if (map.entrySet().stream().mapToInt(Map.Entry::getValue).anyMatch(value -> value == 2)) {
                count2 += 1;
            }

            if (map.entrySet().stream().mapToInt(Map.Entry::getValue).anyMatch(value -> value == 3)) {
                count3 += 1;
            }
        }

        System.out.println("count2 = " + count2);
        System.out.println("count3 = " + count3);
        System.out.println("(count2 * count3) = " + (count2 * count3));
    }

    public static Map<Character, Integer> createMap(String input) {
        Map<Character, Integer> count = new HashMap<>();
        for (char c : input.toCharArray()) {
            if(count.containsKey(c)) {
                count.put(c, count.get(c)+1);
            } else {
                count.put(c, 1);
            }
        }
        
        return count;
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day02.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
