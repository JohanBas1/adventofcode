package be.johanbas.day6;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Johan Bas on 7/12/2016.
 */
public class Day6 {

	public static void main(String[] args) throws IOException, URISyntaxException {

		List<String> input = readInput();
		Map<String, Integer> een = new HashMap<>();
		Map<String, Integer> twee = new HashMap<>();
		Map<String, Integer> drie = new HashMap<>();
		Map<String, Integer> vier = new HashMap<>();
		Map<String, Integer> vijf = new HashMap<>();
		Map<String, Integer> zes = new HashMap<>();
		Map<String, Integer> zeven = new HashMap<>();
		Map<String, Integer> acht = new HashMap<>();

		Map<String, Integer>[] lijst = new Map[]{een, twee, drie, vier, vijf, zes, zeven, acht};

		for (String s : input) {
			for (int j = 0; j < s.length(); j++){



				int i = lijst[j].getOrDefault(s.charAt(j)+"", 0);
				i++;
				lijst[j].put(s.charAt(j)+"", i);
			}
		}

		System.out.print(getMostUsedCharFromMap(een));
		System.out.print(getMostUsedCharFromMap(twee));
		System.out.print(getMostUsedCharFromMap(drie));
		System.out.print(getMostUsedCharFromMap(vier));
		System.out.print(getMostUsedCharFromMap(vijf));
		System.out.print(getMostUsedCharFromMap(zes));
		System.out.print(getMostUsedCharFromMap(zeven));
		System.out.print(getMostUsedCharFromMap(acht));

		System.out.println();

		System.out.print(getleastUsedCharFromMap(een));
		System.out.print(getleastUsedCharFromMap(twee));
		System.out.print(getleastUsedCharFromMap(drie));
		System.out.print(getleastUsedCharFromMap(vier));
		System.out.print(getleastUsedCharFromMap(vijf));
		System.out.print(getleastUsedCharFromMap(zes));
		System.out.print(getleastUsedCharFromMap(zeven));
		System.out.print(getleastUsedCharFromMap(acht));
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day6.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}

	private static String getMostUsedCharFromMap(Map<String, Integer> map) {
		final List<String> collect = map.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getValue))
				.map(Map.Entry::getKey).collect(Collectors.toList());

//		System.out.println(collect);
		Collections.reverse(collect);

		return collect.get(0);
	}

	private static String getleastUsedCharFromMap(Map<String, Integer> map) {
		final List<String> collect = map.entrySet()
				.stream()
				.sorted(Comparator.comparing(Map.Entry::getValue))
				.map(Map.Entry::getKey).collect(Collectors.toList());

		return collect.get(0);
	}
}
