package be.johanbas.y2020.day7;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day07 {

    static Set<String> solutions = new TreeSet<>();
    static Map<String, String> map = new HashMap<>();
    static final String shiny_gold = "shiny gold";

    public static void main(String[] args) throws IOException, URISyntaxException {

        part1();

        final int subCountOf = getSubCountOf(shiny_gold, inputParsedPart2());
        System.out.println("Part2 " + (subCountOf - 1)); // min de gouden zak (door de plus zak zelf)
    }

    private static void part1() throws IOException, URISyntaxException {
        final List<String> strings = readInput();

        strings.forEach(s -> {
            final String[] split = s.split(" bags contain ");
            map.put(split[0], split[1]);
        });

        solutions.addAll(search(Set.of(shiny_gold)));
        int solutionCounter = 0;
        while (solutionCounter != solutions.size()) {
            solutionCounter = solutions.size();
            solutions.addAll(search(solutions));
//            System.out.println("solutions.size() = " + solutions.size());
        }

        System.out.println("Part1 " + solutions.size());
//        System.out.println("solutions = " + solutions);
    }

    private static Set<String> search(Set<String> searchFor) {
        Set<String> result = new TreeSet<>();

        for (Map.Entry<String, String> entry : map.entrySet()) {
            String found = entry.getKey();
            String in = entry.getValue();

            for (String search : searchFor) {
                if (in.contains(search)) {
                    result.add(found);
                }
            }
        }

        return result;
    }

    private static Map<String, Map<String, Integer>> inputParsedPart2() throws IOException, URISyntaxException {
        Map<String, Map<String, Integer>> map = new HashMap<>();
        for (String line : readInput()) {
            // parse (hoofd)zak en sub-zakken
            final String[] split = line.split(" bags contain ");
            final String zak = split[0];
            final String subZakken = split[1];

            // voeg hoofzak toe aan map
            Map<String, Integer> subZakkenMetAantal = map.computeIfAbsent(zak, s -> new HashMap<>());

            // parse sub-zakken
            for (String subBag : subZakken.split(", ")) {
                // https://regex101.com/
                Matcher subMatcher = Pattern.compile("(\\d) (.+) (bag) ?").matcher(subBag);
                if (subMatcher.find()) { // zijn er wel subzakken?
                    subZakkenMetAantal.put(subMatcher.group(2), Integer.parseInt(subMatcher.group(1)));
                }
            }
        }

//        System.out.println("map = " + map);
        return map;
    }

    public static int getSubCountOf(String bag, Map<String, Map<String, Integer>> inputParsed) {

        final Map<String, Integer> stringIntegerMap = inputParsed.get(bag);
        if (stringIntegerMap.size() > 0) {
            int count = 0;
            for (Map.Entry<String, Integer> entry : stringIntegerMap.entrySet()) {
                count += entry.getValue() * getSubCountOf(entry.getKey(), inputParsed);
            }
//            System.out.println(bag + " contains " + count);

            return count + 1; // plus de zak zelf
        } else {
//            System.out.println(bag + " contains " + 0);
            return 1;
        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day07.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
