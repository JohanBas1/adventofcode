package be.johanbas.y2021.day01;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Day1 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        final List<String> strings = readInput();

        final List<Integer> collect = strings.stream().map(Integer::parseInt).collect(Collectors.toList());

        System.out.println("Part1");
        part1(collect);

        List<Integer> slidingList = new LinkedList<>();
        for (int i = 0; i < collect.size()-2; i++) {
            slidingList.add(collect.get(i) + collect.get(i+1) + collect.get(i+2));
        }

        System.out.println("Part2");
        part1(slidingList);
    }

    private static void part1(List<Integer> collect) {
        int counter = 0;
        for (int i = 1; i < collect.size(); i++) {
            if(collect.get(i) > collect.get(i-1)) {
                counter++;
            }
        }

        System.out.println("Increase = " + counter);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day1.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
