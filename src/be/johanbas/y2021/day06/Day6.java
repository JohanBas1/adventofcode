package be.johanbas.y2021.day06;

import java.util.*;
import java.util.stream.Collectors;

public class Day6 {

    public static void main(String[] args) {

//        String inputString = "3,4,3,1,2";
        String inputString = "1,4,3,3,1,3,1,1,1,2,1,1,1,4,4,1,5,5,3,1,3,5,2,1,5,2,4,1,4,5,4,1,5,1,5,5,1,1,1,4,1,5,1,1,1,1,1,4,1,2,5,1,4,1,2,1,1,5,1,1,1,1,4,1,5,1,1,2,1,4,5,1,2,1,2,2,1,1,1,1,1,5,5,3,1,1,1,1,1,4,2,4,1,2,1,4,2,3,1,4,5,3,3,2,1,1,5,4,1,1,1,2,1,1,5,4,5,1,3,1,1,1,1,1,1,2,1,3,1,2,1,1,1,1,1,1,1,2,1,1,1,1,2,1,1,1,1,1,1,4,5,1,3,1,4,4,2,3,4,1,1,1,5,1,1,1,4,1,5,4,3,1,5,1,1,1,1,1,5,4,1,1,1,4,3,1,3,3,1,3,2,1,1,3,1,1,4,5,1,1,1,1,1,3,1,4,1,3,1,5,4,5,1,1,5,1,1,4,1,1,1,3,1,1,4,2,3,1,1,1,1,2,4,1,1,1,1,1,2,3,1,5,5,1,4,1,1,1,1,3,3,1,4,1,2,1,3,1,1,1,3,2,2,1,5,1,1,3,2,1,1,5,1,1,1,1,1,1,1,1,1,1,2,5,1,1,1,1,3,1,1,1,1,1,1,1,1,5,5,1";
        List<Integer> input = Arrays.stream(inputString.split(",")).map(Integer::parseInt).collect(Collectors.toList());


        Map<Integer, Integer> map = new HashMap<>();
        List<Integer> calculate = calculate(List.of(6));
        map.put(6, calculate.size());
        calculate = oneCalculation(calculate);
        map.put(5, calculate.size());
        calculate = oneCalculation(calculate);
        map.put(4, calculate.size());
        calculate = oneCalculation(calculate);
        map.put(3, calculate.size());
        calculate = oneCalculation(calculate);
        map.put(2, calculate.size());
        calculate = oneCalculation(calculate);
        map.put(1, calculate.size());

        System.out.println(" map calculation done ");



        System.out.println("part1 = " + input.stream().map(map::get).reduce(Integer::sum).orElse(0));
    }

    private static List<Integer> calculate(List<Integer> input) {

        for (int i = 0; i < 256; i++) {
            input = oneCalculation(input);
            System.out.println("i = " + i);
        }

        return input;
    }

    private static List<Integer> oneCalculation(List<Integer> input) {
        List<Integer> newInput = new LinkedList<>();
        input.forEach(x -> {
            if (x == 0) {
                newInput.add(8);
                newInput.add(6);
            } else {
                newInput.add(x - 1);
            }
        });
        return newInput;
    }

}
