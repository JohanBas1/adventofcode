package be.johanbas.y2015.day21;

/**
 * Created by Johan Bas.
 */
public class Weapon implements Item {

    private int dmg;
    private int cost;
    private String name;

    public Weapon(int cost, int dmg, String name) {
        this.dmg = dmg;
        this.cost = cost;
        this.name = name;
    }

    public int getDmg() {
        return dmg;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public int getArmor() {
        return 0;
    }

    @Override
    public void setArmor(int armor) {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Weapon weapon = (Weapon) o;

        if (dmg != weapon.dmg) return false;
        if (cost != weapon.cost) return false;
        return name.equals(weapon.name);

    }

    @Override
    public int hashCode() {
        int result = dmg;
        result = 31 * result + cost;
        result = 31 * result + name.hashCode();
        return result;
    }
}
