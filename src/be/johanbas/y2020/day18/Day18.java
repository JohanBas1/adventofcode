package be.johanbas.y2020.day18;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day18 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        final List<String> input = readInput();

        BigInteger sum = BigInteger.ZERO;

        for (String i : input) {
            Expression e = new Expression(i.replaceAll(" ",""));
            BigInteger eval = e.eval();
            System.out.println("eval = " + eval);
            sum = sum.add(eval);
        }

        System.out.println("sum = " + sum);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day18.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}
