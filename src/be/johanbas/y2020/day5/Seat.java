package be.johanbas.y2020.day5;

public class Seat {

    private int rowNumber = 0;
    private int columnNumber = 0;

    public Seat(String input) {
        final char[] rowDescriptor = input.substring(0, 7).toCharArray();
        if(rowDescriptor[0] == 'B')
            rowNumber+=64;
        if(rowDescriptor[1] == 'B')
            rowNumber+=32;
        if(rowDescriptor[2] == 'B')
            rowNumber+=16;
        if(rowDescriptor[3] == 'B')
            rowNumber+=8;
        if(rowDescriptor[4] == 'B')
            rowNumber+=4;
        if(rowDescriptor[5] == 'B')
            rowNumber+=2;
        if(rowDescriptor[6] == 'B')
            rowNumber+=1;

        final char[] columnDescriptor = input.substring(7, 10).toCharArray();
        if(columnDescriptor[0] == 'R')
            columnNumber+=4;
        if(columnDescriptor[1] == 'R')
            columnNumber+=2;
        if(columnDescriptor[2] == 'R')
            columnNumber+=1;

    }

    public int seatId() {
        return (rowNumber * 8) + columnNumber;
    }
}
