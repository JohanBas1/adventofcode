package be.johanbas.y2018.day04;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class Guard {

    List<Log> logs = new LinkedList<>();
    String id;

    public Guard(String id) {
        this.id = id;
    }

    public Integer getMinutesAsleep() {
        boolean awake = true;
        int minutesAsleep = 0;
        LocalDateTime prevTime = null;
        for (Log log : logs) {

            if(!awake) {
                minutesAsleep += (log.stamp.getMinute() - prevTime.getMinute());
            }

            prevTime = log.stamp;
            awake = !awake;
        }

        return minutesAsleep;
    }

    @Override
    public String toString() {
        return "Guard{" +
                "logs=" + logs +
                ", id='" + id + '\'' +
                ", minutes='" + getMinutesAsleep() + '\'' +
                '}';
    }
}
