package be.johanbas.y2022.day01;

import java.util.List;

public record Elf(List<Integer> inventory) {

    public Integer getTotal() {
        return inventory.stream().reduce(Integer::sum).orElse(0);
    }

}