package be.johanbas.y2015.day22;

import java.util.PriorityQueue;

public class Day22 {

    private static PriorityQueue<PlayState> queue = new PriorityQueue<>();

    public static void main(String[] args) throws CloneNotSupportedException {

        // part 1
        boolean part2 = true;

        PlayState ps = new PlayState(50, 500, 55, 8); // 953
//        PlayState ps = new PlayState(10, 250, 13, 8); // 226
//        PlayState ps = new PlayState(10, 250, 14, 8); // 641
        queue.add(ps);

        while(true) {
            // play a round with the most optimal state:
            PlayState first = queue.poll();

            if(part2) {
                if(first.surviveHardmode()) { // 1289
                    play(first);
                }
            } else {
                play(first); // 953
            }

        }
    }

    private static void play(PlayState first) throws CloneNotSupportedException {
        var missiles = false;
        var drain = false;
        var shield = false;
        var poison = false;

        // However, effects can be started on the same turn they end.
        first.handleEffects();

        if(first.canPlayMagicMissile()) {
            missiles = playMissiles(first);
        }
        if(first.canPlayDrain()) {
            drain = playDrain(first);
        }
        if(first.canPlayShield()) {
            shield = playShield(first);
        }
        if(first.canPlayPoison()) {
            poison = playPoison(first);
        }
        if(first.canplayRecharge()) {
            playRecharge(first);
        }

        if(missiles || drain || shield || poison) {
            System.exit(0);
        }
    }

    private static boolean playMissiles(PlayState first) throws CloneNotSupportedException {
        var playState = first.clone();
        if (playState.playMagicMissile()) { // als boss niet dood is spelen we verder
            System.out.println("boss dood in " + playState.getTotalManaSpend());
            return true;
        }
        if(!playState.bossTurn()) { // als speler niet dood is spelen we verder
            queue.add(playState);
        }

        return false;
    }

    private static boolean playDrain(PlayState first) throws CloneNotSupportedException {
        var playState = first.clone();
        if (playState.playDrain()) { // als boss niet dood is spelen we verder
            System.out.println("boss dood in " + playState.getTotalManaSpend());
            return true;
        }
        if(!playState.bossTurn()) { // als speler niet dood is spelen we verder
            queue.add(playState);
        }

        return false;
    }

    private static boolean playShield(PlayState first) throws CloneNotSupportedException {
        var playState = first.clone();

        if(!playState.isShieldActive()) {
            if (playState.playShield()) { // als boss niet dood is spelen we verder
                System.out.println("boss dood in " + playState.getTotalManaSpend());
                return true;
            }
            if (!playState.bossTurn()) { // als speler niet dood is spelen we verder
                queue.add(playState);
            }
        }
        return false;
    }

    private static boolean playPoison(PlayState first) throws CloneNotSupportedException {
        var playState = first.clone();

        if(!playState.isPoisonActive()) {
            if (playState.playPoison()) { // als boss niet dood is spelen we verder
                System.out.println("boss dood in " + playState.getTotalManaSpend());
                return true;
            }
            if (!playState.bossTurn()) { // als speler niet dood is spelen we verder
                queue.add(playState);
            }
        }
        return false;
    }

    private static void playRecharge(PlayState first) throws CloneNotSupportedException {
        var playState = first.clone();

        if(!playState.isRechargeActive()) {
            playState.playRecharge();

            if (!playState.bossTurn()) { // als speler niet dood is spelen we verder
                queue.add(playState);
            }
        }
    }
}
