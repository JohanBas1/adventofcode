package be.johanbas.y2020.day4;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.StringJoiner;

public class Day04 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        final List<String> stringList = readInput();

        StringJoiner sj = new StringJoiner(" ");
        List<String> cleanInput = new LinkedList<>();
        for (String s : stringList) {
            if(s.trim().equals("")) {
                cleanInput.add(sj.toString());
                sj = new StringJoiner(" ");
            } else {
                sj.add(s);
            }
        }
        cleanInput.add(sj.toString());

        System.out.println("part 1 = " + cleanInput.stream().map(Password::new).filter(Password::valid1).count());
        System.out.println("part 2 = " + cleanInput.stream().map(Password::new).filter(Password::valid1).filter(Password::valid2).count());
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day04.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
