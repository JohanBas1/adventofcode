package be.johanbas.y2021.day04;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Day4 {

    public static List<BingoBoard> bingoBoards = new LinkedList<>();

    public static void main(String[] args) throws URISyntaxException, IOException {

        var input = readInput();
        var bingoMoves = input.get(0).split(",");

        input.remove(0);

        bingoBoards.add(new BingoBoard(input.subList(0,6)));
        while(input.size() > 6) {
            input = input.subList(5, input.size());
            bingoBoards.add(new BingoBoard(input.subList(0,6)));
        }

//        part1(bingoMoves);

        BingoBoard lastbb = null;
        for (String bingoMove : bingoMoves) {
            System.out.println("bingoMove = " + bingoMove);
            bingoBoards.forEach(bingoBoard -> bingoBoard.makeMove(Integer.parseInt(bingoMove)));
            bingoBoards = bingoBoards.stream().filter(bb -> !bb.hasBingo()).collect(Collectors.toList());
            if(bingoBoards.size() == 1) {
                lastbb = bingoBoards.get(0);
            }

            if(bingoBoards.size() == 0) {
                final int boardScore = lastbb.calculateBoardScore();
                final int move = Integer.parseInt(bingoMove);
                System.out.println("part2 = " + move * boardScore);
                System.exit(0);
            }
        }
    }

    private static void part1(String[] bingoMoves) {
        for (String bingoMove : bingoMoves) {
            bingoBoards.forEach(bingoBoard -> {
                bingoBoard.makeMove(Integer.parseInt(bingoMove));
                if(bingoBoard.hasBingo()) {
                    final int boardScore = bingoBoard.calculateBoardScore();
                    final int move = Integer.parseInt(bingoMove);
                    System.out.println("part1 = " + move * boardScore);
                    System.exit(0);
                }
            });
        }
    }


    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day4.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
