package be.johanbas.y2017.day09;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Johan Bas on 9/12/2017.
 */
public class Day09 {

	public static void main(String[] args) throws IOException, URISyntaxException {
		// CTRL + ALT + SHIFT + J  op !! en daarna op ! en volgend karakter
		String input = readInput().get(0);
		parseInput(input);

	}

	private static void parseInput(String input) {
//		input = input.replaceAll("[^{}!<>]", "");

		int gabageCount = 0;
		boolean garbage = false;
		int dept = 0;
		int score = 0;

		for (char c : input.toCharArray()) {
			if (c == '{') {
				if(!garbage) {
					dept++;
				} else {
					gabageCount++;
				}

			} else if (c == '}') {
				if(!garbage) {
					score += dept;
					dept--;
				} else {
					gabageCount++;
				}

			} else if (c == '<') {
				if(garbage) {
					gabageCount++;
				}
				garbage = true;

			} else if (c == '>') {
				garbage = false;

			} else {
				if(garbage) {
					gabageCount++;
				}
//				System.out.println("error " + c);
			}
		}

//		System.out.println("garbage = " + garbage);
//		System.out.println("skip = " + skip);
//		System.out.println("dept = " + dept);
		System.out.println("score = " + score);
		System.out.println("gabageCount = " + gabageCount);
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day09.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}
