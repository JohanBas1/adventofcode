package be.johanbas.y2017.day07;

import be.johanbas.y2017.day05.Node;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Johan Bas on 7/12/2017.
 */
public class Day07 {

	private static Map<String,Node> allNodes = new HashMap<>();

	public static void main(String[] args) throws IOException, URISyntaxException {

		// part 1
		parseInput();
		setRelationships();
		Node root = findRootNode();
		System.out.println(root.getName());

		// part 2
		Node parentWithWrongBalence = findNodeWithWrongBalence(root);
		System.out.println("node with wrong weight:" + parentWithWrongBalence);
		Node parent = parentWithWrongBalence.getParent();
		System.out.println("it's parent children to check what weight it needs to get");
		System.out.println("-----");
		for (Node node : parent.getChildren()) {
			System.out.println(node);
		}
	}

	private static Node findRootNode() {
		Iterator<Map.Entry<String, Node>> iterator = allNodes.entrySet().iterator();
		Node root = iterator.next().getValue();
		while(root.getParent() != null) {
			root = root.getParent();
		}
		return root;
	}

	private static void setRelationships() {
		for (Map.Entry<String, Node> node : allNodes.entrySet()) {
			for (String s : node.getValue().getTextChildren()) {
				Node child = allNodes.get(s);
				node.getValue().getChildren().add(child);
				child.setParent(node.getValue());
			}
		}
	}

	private static void parseInput() throws URISyntaxException, IOException {
		List<String> strings = readInput();
		for (String string : strings) {
			String[] split = string.split(" -> ");
			String[] first = split[0].split(" ");

			String name = first[0];
			int weight = Integer.parseInt(first[1].replaceAll("\\(","").replaceAll("\\)",""));
			if(split.length > 1) {
				Node node = new Node(name, weight, new HashSet<>(Arrays.asList(split[1].split(", "))));
				allNodes.put(name,node);
			} else {
				Node node = new Node(name, weight, new HashSet<>());
				allNodes.put(name,node);
			}
		}
	}

	private static Node findNodeWithWrongBalence(Node root) {
		Map<Integer,List<String>> countMap = new HashMap<>();
		for (Node node : root.getChildren()) {
			if(countMap.get(node.getBalence()) != null) {
				countMap.get(node.getBalence()).add(node.getName());
			} else {
				List<String> names = new LinkedList<>();
				names.add(node.getName());
				countMap.put(node.getBalence(),names);
			}
		}

		if(countMap.entrySet().size() == 1) {
//			System.out.println("get parent from one of these " + countMap + ", its weight is wrong");
			return allNodes.get(countMap.entrySet().stream().findFirst().get().getValue().get(0)).getParent();
		} else {
			for (Map.Entry<Integer, List<String>> integerSetEntry : countMap.entrySet()) {
				if(integerSetEntry.getValue().size() == 1) {
					return findNodeWithWrongBalence(allNodes.get(integerSetEntry.getValue().get(0)));
				}
			}
		}

		return null;
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day07.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}
