package be.johanbas.y2015.day25;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;

public class Day25 {

    public static void main(String[] args) {

        long start = System.currentTimeMillis();

        int row = 2978;
        int column = 3083;
        int index = calculateCodePositionInList(column, row);
        System.out.println("Get element at:"+ index);
        var seed = BigInteger.valueOf(20151125);
        var multiply = BigInteger.valueOf(252533);
        var divide = BigInteger.valueOf(33554393);

        List<BigInteger> values = new LinkedList<>();
        values.add(seed);
        while(values.size() < index+1) {
            seed = seed.multiply(multiply).remainder(divide);
            values.add(seed);
        }

        System.out.println("Elements calculated in "+(System.currentTimeMillis() - start)+"ms");

        System.out.println("Code:"+values.get(calculateCodePositionInList(column,row)));
    }

    // determine first in column
    public static int gaussSum(int n) {
        return (n*(n+1))/2;
    }

    public static int calculateCodePositionInList(int column, int row) {
        int n = gaussSum(column);

        for(int i=0; i<row-1; i++) {
            n=n+column+i;
        }

        return n-1;
    }
}
