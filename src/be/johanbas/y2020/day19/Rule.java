package be.johanbas.y2020.day19;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Rule {
    private final String ruleNumber;
    private final List<RulePart> partList;

    public Rule(String ruleNumber, String ruleValue) {
        this.ruleNumber = ruleNumber;
        partList = Arrays.stream(ruleValue.split(" ")).map(Rule::createPart).toList();
    }

    public String getRuleNumber() {
        return ruleNumber;
    }

    public String computeRule(Map<String, Rule> allRules) {
        var evaluatedRule = partList.stream().map(part -> evaluatePart(part, allRules)).collect(Collectors.joining());

        if(partList.stream().anyMatch(rulePart -> rulePart instanceof  PipePart))
            return String.format("(%s)", evaluatedRule);
        else
            return evaluatedRule;
    }

    private String evaluatePart(RulePart part, Map<String, Rule> allRules) {
        return switch (part) {
            case RuleNumberPart ruleNumberPart -> allRules.get(ruleNumberPart.value()).computeRule(allRules);
            case CharacterPart characterPart -> characterPart.getValue();
            case PipePart pipePart -> pipePart.getValue();
        };
    }

    private static RulePart createPart(String input) {
        if(input.contains("|")) {
            return new PipePart();
        } else if(input.contains("a")) {
            return new APart();
        } else if(input.contains("b")) {
            return new BPart();
        } else {
            return new RuleNumberPart(input);
        }
    }

    private sealed interface RulePart permits PipePart, CharacterPart, RuleNumberPart  { String getValue(); }
    private record PipePart() implements RulePart { public String getValue() { return "|"; } }
    private record RuleNumberPart(String value) implements RulePart { public String getValue() { return value; } }
    private sealed interface CharacterPart extends RulePart permits APart, BPart {}
        private record APart() implements CharacterPart { public String getValue() { return "a"; } }
        private record BPart() implements CharacterPart { public String getValue() { return "b"; } }
}
