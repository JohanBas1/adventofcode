package be.johanbas.y2015.day11;

import java.util.Set;
import java.util.TreeSet;

/*
--- Day 11: Corporate Policy ---

Santa's previous password expired, and he needs help choosing a new one.

To help him remember his new password after the old one expires, Santa has devised a method of coming up with a password based on the previous one.
Corporate policy dictates that passwords must be exactly eight lowercase letters (for security reasons),
so he finds his new password by incrementing his old password string repeatedly until it is valid.

Incrementing is just like counting with numbers: xx, xy, xz, ya, yb, and so on.
Increase the rightmost letter one step; if it was z, it wraps around to a, and repeat with the next letter to the left until one doesn't wrap around.

Unfortunately for Santa, a new Security-Elf recently started, and he has imposed some additional password requirements:

1. Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
2. Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
3. Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.
For example:

hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
abbcegjk fails the third requirement, because it only has one double letter (bb).
The next password after abcdefgh is abcdffaa.
The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.
Given Santa's current password (your puzzle input), what should his next password be?

--- Part Two ---

Santa's password expired again. What's the next one?

 */
public class Day11 {

    private static String ALPHABET = "abcdefghijklmnopqrstuvwxyz";

    public static void main(String[] args) {
        String input = "hepxcrrq";

        long time = System.currentTimeMillis();

        Set<String> substrings = new TreeSet<>();
        for(int i=0; i<24; i++) {
            substrings.add(ALPHABET.substring(i, i+3));
        }

        System.out.println("substring: " + (System.currentTimeMillis() - time) + "ms");
        time = System.currentTimeMillis();

        Set<String> doubles = new TreeSet<>();
        for(int i=0; i<26; i++) {
            doubles.add(ALPHABET.substring(i,i+1)+ALPHABET.substring(i,i+1));
        }
        System.out.println("doubles: " + (System.currentTimeMillis() - time) + "ms");
        time = System.currentTimeMillis();

        int length = input.length()-1;
        char[] chars = input.toCharArray();

        String pwd1 = generatePWD(substrings, doubles, length, chars);

        System.out.println("calculation: " + (System.currentTimeMillis() - time) + "ms");
        time = System.currentTimeMillis();

        length = pwd1.length()-1;
        chars = pwd1.toCharArray();

        generatePWD(substrings, doubles, length, chars);
        System.out.println("calculation: " + (System.currentTimeMillis() - time) + "ms");
    }

    private static String generatePWD(Set<String> substrings, Set<String> doubles, int length, char[] chars) {
        while(true) {
            nextPWD(length, chars);
            String s = String.valueOf(chars);
//            System.out.println(s);
            if(firstRequirement(s,substrings) && secondRequirement(s) && thirdRequirement(s,doubles)) {
                System.out.println(s);
                return s;
            }
        }
    }

    private static void nextPWD(int length, char[] chars) {

        char aChar = chars[length];
        if(aChar == 'z') {
            chars[length] = 'a';
            nextPWD(length-1, chars);
        } else {
            updateCharAt(length, chars);
        }
    }

    private static void updateCharAt(int length, char[] chars) {
        chars[length] = (char) (chars[length] +1);
    }


    // Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
    private static boolean firstRequirement(String challenge, Set<String> substrings) {
        for (String substring : substrings) {
            if(challenge.contains(substring)) {
                return true;
            }
        }
        return false;
    }

    // Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
    private static boolean secondRequirement(String challenge) {
        if(challenge.contains("i") || challenge.contains("o") || challenge.contains("l")) {
            return false;
        }
        return true;
    }

    // Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.
    private static boolean thirdRequirement(String challenge, Set<String> doubles) {
        int count=0;
        for (String aDouble : doubles) {
            if(challenge.contains(aDouble)) {
                count++;
            }
            if(count > 1) {
                return true;
            }
        }
        return false;
    }
}
