package be.johanbas.y2022.day01;

import com.google.common.base.Strings;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Day01 {

    public static void main(String[] args) throws URISyntaxException, IOException {

        var total = new LinkedList<Elf>();
        var currentElf = new Elf(new LinkedList<>());
        total.add(currentElf);
        for (var i : readInput()) {
            if(Strings.isNullOrEmpty(i)) {
                currentElf = new Elf(new LinkedList<>());
                total.add(currentElf);
            } else {
                currentElf.inventory().add(Integer.parseInt(i));
            }
        }

        System.out.println("part1 = " + total.stream().map(Elf::getTotal).max(Integer::compareTo));

        var sorted = total.stream()
                .map(Elf::getTotal)
                .sorted(Comparator.reverseOrder())
                .limit(3)
                .reduce(Integer::sum);

        System.out.println("part2 = " + sorted);

    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        var input = Day01.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
