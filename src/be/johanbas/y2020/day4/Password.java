package be.johanbas.y2020.day4;


import java.util.HashMap;
import java.util.Map;

/**
 * byr (Birth Year)
 * iyr (Issue Year)
 * eyr (Expiration Year)
 * hgt (Height)
 * hcl (Hair Color)
 * ecl (Eye Color)
 * pid (Passport ID)
 * cid (Country ID)
 */
public class Password {

    Map<String,String> map = new HashMap<>();

    public Password(String input) {
        final String[] words = input.split(" ");
        for (String word : words) {
            final String[] chunk = word.split(":");
            map.put(chunk[0], chunk[1]);
        }
    }

    public boolean valid1() {
        return map.containsKey("byr") &&
                map.containsKey("iyr") &&
                map.containsKey("eyr") &&
                map.containsKey("hgt") &&
                map.containsKey("hcl") &&
                map.containsKey("ecl") &&
                map.containsKey("pid");

    }

    public boolean valid2() {

        final int byr = Integer.parseInt(map.get("byr"));
        if(byr < 1920 || byr > 2002) {
            return false;
        }

        final int iyr = Integer.parseInt(map.get("iyr"));
        if (iyr < 2010 || iyr > 2020) {
            return false;
        }

        final int eyr = Integer.parseInt(map.get("eyr"));
        if (eyr < 2020 || eyr > 2030) {
            return false;
        }

        final String hgt = map.get("hgt");
        if(hgt.endsWith("cm")) {
            final int cm = Integer.parseInt(hgt.replace("cm", ""));
            if(cm < 150 || cm > 193) {
                return false;
            }
        } else if(hgt.endsWith("in")) {
            final int in = Integer.parseInt(hgt.replace("in", ""));
            if(in < 59 || in > 76) {
                return false;
            }
        } else {
            return false;
        }

        final String hcl = map.get("hcl");
        if(!hcl.matches("#([0-9a-f]{6})")) {
            return false;
        }

        final String ecl = map.get("ecl");
        if(!(
                ecl.equals("amb") ||
                        ecl.equals("blu") ||
                        ecl.equals("brn") ||
                        ecl.equals("gry") ||
                        ecl.equals("grn") ||
                        ecl.equals("hzl") ||
                        ecl.equals("oth")

                )) {
            return false;
        }

        final String pid = map.get("pid");
        if(!pid.matches("\\d{9}")){
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "Password{" +
                "map=" + map +
                '}';
    }
}
