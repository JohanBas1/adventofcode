package be.johanbas.y2020.day3;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day03 {

    static int TREE_COUNTER = 0;
    static int X = 0;
    static BigInteger result2 = BigInteger.ZERO;

    public static void main(String[] args) throws IOException, URISyntaxException {
        final List<String> strings = readInput();
        strings.remove(0);

        boolean skip = true;
        for (String string : strings) {
            if(!skip) {
                processLine(string, 1);
            }
            skip = !skip;
        }
        System.out.println("TREE_COUNTER = " + TREE_COUNTER );
        result2 = new BigInteger(String.valueOf(TREE_COUNTER));
        TREE_COUNTER = 0;
        X = 0;


        final List<Integer> slopes = List.of(1, 3, 5, 7);

        for (Integer slope : slopes) {
            for (String string : strings) {
                processLine(string, slope);
            }

            System.out.println("TREE_COUNTER = " + TREE_COUNTER + " slope offset: " + slope);
            result2 = result2.multiply(new BigInteger(String.valueOf(TREE_COUNTER)));
            TREE_COUNTER = 0;
            X = 0;
        }

        System.out.println("result2 = " + result2);
    }

    private static void processLine(String string, int xOffset) {
        final char[] chars = string.toCharArray();
        X = (X + xOffset) % string.length();
        if(chars[X] == '#') {
            TREE_COUNTER++;
        }
//        System.out.println(string + " " + X);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day03.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
