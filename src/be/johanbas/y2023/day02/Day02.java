package be.johanbas.y2023.day02;

import be.johanbas.tools.Filez;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Day02 {

    public static void main(String[] args) {
        var input = Filez.readFile("input.txt", Day02.class);

        var games = input.stream().map(Day02::parse).toList();

        var part1 = games.stream().filter(Game::isValidGame).map(Game::number).reduce(Integer::sum).orElse(-1);
        System.out.println("part1 = " + part1);

        var part2 = games.stream().map(Game::minimumCubes).reduce(BigInteger::add).orElse(BigInteger.ZERO);
        System.out.println("part2 = "+part2);
    }

    private static Game parse(String it) {
        // Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        var regex = "Game (?<gamenumber>\\d+): (?<games>.*)";
        var matcher = Pattern.compile(regex).matcher(it);
        if (matcher.matches()) {
            // Access named groups using group(name)
            var gameNumber = Integer.parseInt(matcher.group("gamenumber"));
            var games = matcher.group("games").split("; ");
            var list = Arrays.stream(games).map(Day02::parseDraw).toList();

            return new Game(gameNumber, list);
        } else {
            throw new IllegalArgumentException("Can not parse "+it);
        }
    }

    private static Draw parseDraw(String it) {
        var split = it.split(", ");

        var green = 0;
        var blue = 0;
        var red = 0;

        for (var s : split) {
            var numberDice = s.split(" ");
            if (numberDice[1].equals("green")) {
                green = Integer.parseInt(numberDice[0]);
            } else if (numberDice[1].equals("blue")) {
                blue = Integer.parseInt(numberDice[0]);
            }
            if (numberDice[1].equals("red")) {
                red = Integer.parseInt(numberDice[0]);
            }
        }

        return new Draw(blue, red, green);
    }
}

record Game(int number, List<Draw> draws) {
    boolean isValidGame() {
        return draws.stream().allMatch(Draw::isValidDraw);
    }

    BigInteger minimumCubes() {
        var blue = draws().stream().map(Draw::blue).reduce(Integer::max).orElse(-1);
        var red = draws().stream().map(Draw::red).reduce(Integer::max).orElse(-1);
        var green = draws().stream().map(Draw::green).reduce(Integer::max).orElse(-1);

        return new BigInteger(String.valueOf(blue * red * green));
    }
}

record Draw(int blue, int red, int green) {
    boolean isValidDraw() {
        return red < 13 && green < 14 && blue < 15;
    }
}