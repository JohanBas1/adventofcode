package be.johanbas.y2021.day02;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day2 {

    public static void main(String[] args) throws URISyntaxException, IOException {

        final List<String> strings = readInput();

//        part1(strings);

        int dept = 0;
        int horizontal = 0;
        int aim = 0;

        for (String string : strings) {
            final String[] split = string.split(" ");

            if(split[0].equals("forward")) {
                horizontal += Integer.parseInt(split[1]);
                dept += (Integer.parseInt(split[1]) * aim);
            } else if (split[0].equals("down")) {
                aim += Integer.parseInt(split[1]);
            } else {
                aim -= Integer.parseInt(split[1]);
            }
            System.out.println("");
        }

        System.out.println("part2 " + (dept * horizontal));
    }

    private static void part1(List<String> strings) {
        int dept = 0;
        int horizontal = 0;

        for (String string : strings) {
            final String[] split = string.split(" ");

            if(split[0].equals("forward")) {
                horizontal += Integer.parseInt(split[1]);
            } else if (split[0].equals("down")) {
                dept += Integer.parseInt(split[1]);
            } else {
                dept -= Integer.parseInt(split[1]);
            }
        }

        System.out.println("part1 " + (dept * horizontal));
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day2.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}
