package be.johanbas.y2015.day23;

public class Instruction {

    String input;
    public Instruction(String input) {
        this.input = input;
    }

    public CpuState apply(CpuState cpuState) {

        String[] s = input.split(" ");

        //  hlf r sets register r to half its current value, then continues with the next instruction.
        if(s[0].startsWith("hlf")) {
            if(s[1].equals("a")) {
                return new CpuState(cpuState.a()/2, cpuState.b(), cpuState.offset()+1);
            } else {
                return new CpuState(cpuState.a(), cpuState.b()/2, cpuState.offset()+1);
            }

            // tpl r sets register r to triple its current value, then continues with the next instruction.
        } else if(s[0].startsWith("tpl")) {
            if(s[1].equals("a")) {
                return new CpuState(cpuState.a()*3, cpuState.b(), cpuState.offset()+1);
            } else {
                return new CpuState(cpuState.a(), cpuState.b()*3, cpuState.offset()+1);
            }

            // inc r increments register r, adding 1 to it, then continues with the next instruction.
        } else if(s[0].startsWith("inc")) {
            if(s[1].equals("a")) {
                return new CpuState(cpuState.a()+1, cpuState.b(), cpuState.offset()+1);
            } else {
                return new CpuState(cpuState.a(), cpuState.b()+1, cpuState.offset()+1);
            }

            // jmp offset is a jump; it continues with the instruction offset away relative to itself.
        } else if(s[0].startsWith("jmp")) {
            return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+Integer.parseInt(s[1]));

            // jie r, offset is like jmp, but only jumps if register r is even ("jump if even").
        } else if(s[0].startsWith("jie")) {
            if(s[1].startsWith("a") && cpuState.a()%2 == 0) {
                return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+Integer.parseInt(s[2]));
            }
            if(s[1].startsWith("b") && cpuState.b()%2 == 0) {
                return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+Integer.parseInt(s[2]));
            }
            return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+1);

            // jio r, offset is like jmp, but only jumps if register r is 1 ("jump if one", not odd).
        } else if(s[0].startsWith("jio")) {
            if(s[1].startsWith("a") && cpuState.a() == 1) {
                return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+Integer.parseInt(s[2]));
            }
            if(s[1].startsWith("b") && cpuState.b() == 1) {
                return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+Integer.parseInt(s[2]));
            }
            return new CpuState(cpuState.a(), cpuState.b(), cpuState.offset()+1);

        } else {
            System.out.println("Error!!");
            return new CpuState(cpuState.a(), cpuState.b(), -1);
        }
    }
}
