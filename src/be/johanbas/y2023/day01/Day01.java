package be.johanbas.y2023.day01;

import be.johanbas.tools.Filez;

import java.util.Map;

public class Day01 {

    public static void main(String[] args) {

        var input = Filez.readFile("input.txt", Day01.class);

        System.out.println("part1 = " + input.stream()
                .map(Day01::part1)
                .reduce(Integer::sum).orElse(-1));

        System.out.println("Part2 = " + input.stream()
                .map(Day01::part2)
                .reduce(Integer::sum).orElse(-1));
    }

    public static int part2(String it) {
        var wordToDigit = Map.of("one", "1", "two", "2", "three", "3",
                "four", "4", "five", "5", "six", "6", "seven", "7",
                "eight", "8", "nine", "9"
        );

        var newString = new StringBuilder();
        for(var i=0; i<it.length(); i++) {
            if(Character.isDigit(it.charAt(i))) {
                newString.append(it.charAt(i));
                continue;
            }

            for(var entry : wordToDigit.entrySet()) {
                if(it.substring(i).startsWith(entry.getKey())) {
                    newString.append(entry.getValue());
                    break;
                }
            }
        }

        return Integer.parseInt(String.format("%s%s", newString.charAt(0), newString.charAt(newString.length()-1)));
    }

    public static Integer part1(String it) {
        Character first = null;
        Character last = null;
        for (var c : it.toCharArray()) {
            if(Character.isDigit(c)) {
                if(first == null) {
                    first = c;
                }
                last = c;
            }
        }
        return Integer.parseInt(String.format("%s%s", first, last));
    }
}
