package be.johanbas.y2022.day07;

import be.johanbas.tools.Filez;

import java.util.LinkedList;
import java.util.List;

public class Day07 {

    public static void main(String[] args) {
        var root = new Directory(true, "Root", new LinkedList<>(), new LinkedList<>(), null);
        var currentDir = root;
        var allDirectories = new LinkedList<Directory>();
        allDirectories.add(root);

        var input = Filez.readFile("input.txt", Day07.class);

        for (var s : input) {
            if(s.equals("$ cd /")) {
                currentDir = root;
            } else if(s.equals("$ cd ..")) {
                currentDir = currentDir.parent();
            } else if(s.startsWith("$ cd")) {
                var dir = s.replace("$ cd ", "");
                currentDir = currentDir.subDirectories().stream().filter(d -> d.name().equals(dir)).findAny().orElseThrow();
            } else if(s.equals("$ ls")) {
                // read mode
            } else {
                // ls
                if(s.startsWith("dir")) { // directory
                    var name = s.replace("dir ","");
                    var d = new Directory(false, name, new LinkedList<>(), new LinkedList<>(), currentDir);
                    allDirectories.add(d);
                    currentDir.subDirectories().add(d);
                } else { // file
                    var x = s.split(" ");
                    currentDir.files().add(new File(x[1], Integer.parseInt(x[0])));
                }
            }
        }

        System.out.println("Parsing done");

        var part1 = allDirectories.stream().map(Directory::getSize).filter(size -> size <= 100_000).reduce(Integer::sum).orElse(0);
        System.out.println("part1 = " + part1);

        var totalDisk = 70_000_000;
        var freeNeeded = 30_000_000;
        var currentFree = totalDisk - root.getSize();
        var minSpaceToDelete = freeNeeded - currentFree;

        var directory = allDirectories.stream().filter(d -> d.getSize() > minSpaceToDelete).sorted().findFirst().orElseThrow();
        System.out.println("part2: " + directory.getSize());
    }
}

record Directory(boolean isRoot, String name, List<Directory> subDirectories, List<File> files, Directory parent) implements Comparable<Directory> {

    public Integer getSize() {
        return files.stream().map(be.johanbas.y2022.day07.File::size).reduce(Integer::sum).orElse(0) +
                subDirectories.stream().map(Directory::getSize).reduce(Integer::sum).orElse(0);
    }

    @Override
    public int compareTo(Directory o) {
        return this.getSize().compareTo(o.getSize());
    }
}

record File(String name, int size) {}