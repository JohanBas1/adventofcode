package be.johanbas.y2020.day1;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day01 {

    public static void main(String[] args) throws IOException, URISyntaxException {

//        final List<String> strings = readInput();
//        for (int i = 0; i < strings.size(); i++) {
//            for (int j = i; j < strings.size(); j++) {
//                if((Integer.parseInt(strings.get(i)) + Integer.parseInt(strings.get(j))) == 2020) {
//                    System.out.println(strings.get(i) + " + " + strings.get(j));
//                    System.out.println((Integer.parseInt(strings.get(i)) * Integer.parseInt(strings.get(j))));
//                    break;
//                }
//            }
//        }


        final List<String> strings = readInput();
        for (int i = 0; i < strings.size(); i++) {
            for (int j = i; j < strings.size(); j++) {
                for (int k = j; k < strings.size(); k++) {
                    if ((Integer.parseInt(strings.get(i)) + Integer.parseInt(strings.get(j)) + (Integer.parseInt(strings.get(k)))) == 2020) {
                        System.out.println(strings.get(i) + " + " + strings.get(j) + " + " + strings.get(j));
                        System.out.println((Integer.parseInt(strings.get(i)) * Integer.parseInt(strings.get(j))) * Integer.parseInt(strings.get(k)));
                    }
                }
            }
        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day01.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
