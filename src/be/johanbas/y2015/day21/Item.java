package be.johanbas.y2015.day21;

/**
 * Created by Johan Bas.
 */
public interface Item {

    public int getDmg();

    public void setDmg(int dmg);

    public int getCost();

    public void setCost(int cost);

    public int getArmor();

    public void setArmor(int armor);
}
