package be.johanbas.y2015.day15;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/*

--- Day 15: Science for Hungry People ---

Today, you set out on the task of perfecting your milk-dunking cookie recipe. All you have to do is find the right balance of ingredients.

Your recipe leaves room for exactly 100 teaspoons of ingredients.
You make a list of the remaining ingredients you could use to finish the recipe (your puzzle input) and their properties per teaspoon:

capacity (how well it helps the cookie absorb milk)
durability (how well it keeps the cookie intact when full of milk)
flavor (how tasty it makes the cookie)
texture (how it improves the feel of the cookie)
calories (how many calories it adds to the cookie)
You can only measure ingredients in whole-teaspoon amounts accurately, and you have to be accurate so you can reproduce your results in the future.
The total score of a cookie can be found by adding up each of the properties (negative totals become 0) and then multiplying together everything except calories.

For instance, suppose you have these two ingredients:

Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3

Then, choosing to use 44 teaspoons of butterscotch and 56 teaspoons of cinnamon
(because the amounts of each ingredient must add up to 100) would result in a cookie with the following properties:

A capacity of 44*-1 + 56*2 = 68
A durability of 44*-2 + 56*3 = 80
A flavor of 44*6 + 56*-2 = 152
A texture of 44*3 + 56*-1 = 76

Multiplying these together (68 * 80 * 152 * 76, ignoring calories for now) results in a total score of 62842880,
which happens to be the best score possible given these ingredients. If any properties had produced a negative total, it would have instead become zero,
causing the whole score to multiply to zero.

Given the ingredients in your kitchen and their properties, what is the total score of the highest-scoring cookie you can make?

Sugar: capacity 3, durability 0, flavor 0, texture -3, calories 2
Sprinkles: capacity -3, durability 3, flavor 0, texture 0, calories 9
Candy: capacity -1, durability 0, flavor 4, texture 0, calories 1
Chocolate: capacity 0, durability 0, flavor -2, texture 2, calories 8

 */
public class Day15 {

    static int maxTotalScore = 0;
    static List<Integer> caloriesScore = new LinkedList<>();

    public static void main(String[] args) {


        for(int suggar=1; suggar<100; suggar++) {
            for(int sprinkles=1; sprinkles<100-suggar; sprinkles++) {
                for(int candy=1; candy<100-suggar-sprinkles; candy++) {
                    final int chocolate = 100 - suggar - sprinkles - candy;

                    if(suggar + sprinkles + candy + chocolate == 100) {

//                        System.out.println(suggar + "+" + sprinkles + "+" + candy + "+" + chocolate +"=" + (suggar+sprinkles+candy+chocolate) );

                        /*
                            Sugar:      capacity 3,  durability 0, flavor 0,  texture -3, calories 2
                            Sprinkles:  capacity -3, durability 3, flavor 0,  texture 0,  calories 9
                            Candy:      capacity -1, durability 0, flavor 4,  texture 0,  calories 1
                            Chocolate:  capacity 0,  durability 0, flavor -2, texture 2,  calories 8
                         */

                        int capacity =      (suggar * 3)  + (sprinkles * -3) + (candy * -1) + (chocolate * 0);
                        int durability =    (suggar * 0)  + (sprinkles * 3)  + (candy * 0)  + (chocolate * 0);
                        int flavor =        (suggar * 0)  + (sprinkles * 0)  + (candy * 4)  + (chocolate * -2);
                        int texture =       (suggar * -3) + (sprinkles * 0)  + (candy * 0)  + (chocolate * 2);
                        int calories =      (suggar * 2)  + (sprinkles * 9)  + (candy * 1)  + (chocolate * 8);

                        if(capacity < 0) capacity = 0;
                        if(flavor < 0) flavor = 0;
                        if(texture < 0) texture = 0;

                        int result = capacity * durability * flavor * texture;



                        if(result > maxTotalScore) {
//                            System.out.println(suggar + "+" + sprinkles + "+" + candy + "+" + chocolate +"=" + (suggar+sprinkles+candy+chocolate) );
//                            System.out.println("capacity:" + capacity + " durability:" + durability + " flavor:"+flavor+ " texture:"+ texture + "   +result = " + result);
                            maxTotalScore = result;
                        }

                        if(calories == 500) {
                            caloriesScore.add(result);
                        }
                    }
                }
            }
        }


        System.out.println("maxTotalScore = " + maxTotalScore);
        final Optional<Integer> max = caloriesScore.stream().max(Integer::compareTo);
        System.out.println("max = " + max.get());

        // 362880 te hoog
    }


}