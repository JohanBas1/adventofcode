package be.johanbas.y2020.day9;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Day09 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        var input = readInput();

//        part1(input);
        part2(input);
    }

    private static void part2(List<BigInteger> input) {
        for (int i = 0; i < input.size(); i++) {
            if(checkContiguous(input.subList(i, input.size()))) {
                System.out.println("start = " + i);
                break;
            }
        }

        // resultaat voorbeeld
//        int counter = 4;
//        int start = 2;

        // resultaat input
        int counter = 17;
        int start = 505;

        final List<BigInteger> bigIntegers = input.subList(start, start + counter);
        final BigInteger max = bigIntegers.stream().max(BigInteger::compareTo).orElse(BigInteger.ZERO);
        System.out.println(max);
        final BigInteger min = bigIntegers.stream().min(BigInteger::compareTo).orElse(BigInteger.ZERO);
        System.out.println(min);

        System.out.println("result = " + max.add(min));
    }

    private static boolean checkContiguous(List<BigInteger> subList) {

        BigInteger subSum = BigInteger.ZERO;
//        BigInteger target = new BigInteger("127");
        BigInteger target = new BigInteger("556543474");
        int counter = 0;

        for (BigInteger bigInteger : subList) {
            counter++;
            subSum = subSum.add(bigInteger);
            if(subSum.equals(target)) {
                System.out.println("counter = " + counter);
                return true;
            } else {
                if(subSum.compareTo(target) > 0) {
                    return false;
                }
            }
        }

        return false;
    }

    private static void part1(List<BigInteger> input) {
        int offset = 25;

        for (int i = offset; i < input.size(); i++) {
            if(!isSumOf(input.get(i), input.subList(i - offset, i))) {
                System.out.println(input.get(i) + " is not a sum.");
            }
        }
    }

    private static boolean isSumOf(BigInteger sum, List<BigInteger> list) {
        for (int i = 0; i < list.size(); i++) {
            for (int j = i; j < list.size(); j++) {
                if(list.get(i).add(list.get(j)).equals(sum)) {
                    return true;
                }
            }
        }

        return false;
    }

    private static List<BigInteger> readInput() throws URISyntaxException, IOException {
        URI input = Day09.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input)).stream().map(BigInteger::new).collect(Collectors.toList());
    }
}
