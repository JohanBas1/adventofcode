package be.johanbas.y2020.day15;

import be.johanbas.y2015.day03.Point;

import java.util.*;

public class Day15 {



    public static void main(String[] args) {

        Map<Integer, List<Integer>> mapPrevAndPrevPrev = new HashMap<>();

        List<Integer> list = new LinkedList<>(List.of(2,0,1,9,5,19));
//        List<Integer> list = new LinkedList<>(List.of(0,3,6));
        for (int i = 0; i < list.size(); i++) {
            mapPrevAndPrevPrev.put(list.get(i), new LinkedList<>(List.of(i)));
        }

        int turn = list.size();
        while(turn < 30_000_001) {
//        while(turn < 2021) {
            final Integer prev = list.get(turn - 1);
            if (mapPrevAndPrevPrev.containsKey(prev) && mapPrevAndPrevPrev.get(prev).size() > 1) {

                final List<Integer> prevSpoken = mapPrevAndPrevPrev.get(prev);
                int diff = prevSpoken.get(prevSpoken.size()-1) - prevSpoken.get(prevSpoken.size()-2);
                list.add(diff);

                if (mapPrevAndPrevPrev.containsKey(diff)) {
                    mapPrevAndPrevPrev.get(diff).add(turn);
                } else {
                    mapPrevAndPrevPrev.put(diff, new LinkedList<>(List.of(turn)));
                }

            } else {
                // first time spoken
                list.add(0);
                mapPrevAndPrevPrev.get(0).add(turn);
            }
            turn++;
        }

        System.out.println("a = " + list.get(2020-1));
        System.out.println("a = " + list.get(30_000_000-1));

        // 0 3 6 0 3 3 1 0 4 0

    }

}
