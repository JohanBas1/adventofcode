package be.johanbas.y2015.day09;

import com.google.common.collect.Collections2;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Johan Bas on 9/12/2015.
 */

// https://nl.wikipedia.org/wiki/Handelsreizigersprobleem
public class Day9 {


    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> input = readInput();

        Map<String, Integer> distances = new HashMap<>();
        Set<String> cities = new HashSet<>();

        for (String s : input) {
//            System.out.println(s);
            String[] split = s.split(" ");

            cities.add(split[0]);
            cities.add(split[2]);

            // distances go both ways
            distances.put(split[0]+split[2], Integer.parseInt(split[4]));
            distances.put(split[2]+split[0], Integer.parseInt(split[4]));
        }

        int minValue = Integer.MAX_VALUE;
        int maxValue = Integer.MIN_VALUE;

        // exhaust all possible options b permutating cities order of visit
        for(List<String> possibility : Collections2.permutations(cities)) {
            int total = 0;

            for (int i = 0; i < possibility.size() -1; i++) {
                total += distances.get(possibility.get(i)+possibility.get(i+1));
            }

            if (total < minValue) {
                minValue = total;
            }
            if(total > maxValue) {
                maxValue = total;
            }
        }

        System.out.println(minValue);
        System.out.println(maxValue);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day9.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
