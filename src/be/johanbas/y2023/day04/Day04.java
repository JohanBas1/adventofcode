package be.johanbas.y2023.day04;

import be.johanbas.tools.Filez;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day04 {

    public static Pattern pattern = Pattern.compile("^(Card\\s+(?<cardnumber>\\d+)):\\s+" +
            "(?<n1>\\d+)\\s+(?<n2>\\d+)\\s+(?<n3>\\d+)\\s+(?<n4>\\d+)\\s+(?<n5>\\d+)\\s+" +
            "(?<n6>\\d+)\\s+(?<n7>\\d+)\\s+(?<n8>\\d+)\\s+(?<n9>\\d+)\\s+(?<n10>\\d+)\\s" +
            "\\|\\s+" +
            "(?<n11>\\d+)\\s+(?<n12>\\d+)\\s+(?<n13>\\d+)\\s+(?<n14>\\d+)\\s+(?<n15>\\d+)\\s+(?<n16>\\d+)\\s+(?<n17>\\d+)\\s+(?<n18>\\d+)"
          +  "\\s+(?<n19>\\d+)\\s+(?<n20>\\d+)" +
            "\\s+(?<n21>\\d+)\\s+(?<n22>\\d+)\\s+(?<n23>\\d+)\\s+(?<n24>\\d+)\\s+(?<n25>\\d+)\\s+(?<n26>\\d+)\\s+(?<n27>\\d+)\\s+(?<n28>\\d+)\\s+(?<n29>\\d+)\\s+(?<n30>\\d+)" +
            "\\s+(?<n31>\\d+)\\s+(?<n32>\\d+)\\s+(?<n33>\\d+)\\s+(?<n34>\\d+)\\s+(?<n35>\\d+)"
    );


    public static void main(String[] args) {
        var input = Filez.readFile("input.txt", Day04.class);

        var cards = input.stream().map(Day04::parseLine).toList();
//        cards.forEach(c -> System.out.printf("%s: %s%n", c.cardNumber, c.calculatePoints()));
        System.out.println("Part1 = "+cards.stream().map(Card::calculatePoints).reduce(Integer::sum).orElse(0));

        var cardMap = cards.stream().collect(Collectors.toMap(Card::cardNumber, Function.identity()));

        var queue = new LinkedList<Card>();
        queue.addAll(cards);

        var count = 0;
        while(!queue.isEmpty()) {
            count++;
            var card = queue.removeFirst();
            var matchingNumbers = card.getMatchingNumbers();
            for (var i = 1; i <= matchingNumbers; i++) {
                queue.add(cardMap.get(card.cardNumber()+i));
            }
        }
        System.out.println("part2 = " + count);
    }

    private static Card parseLine(String input) {
        var matcher = pattern.matcher(input);
        if (matcher.matches()) {
            var number = getNx(matcher, "cardnumber");

            var winning = List.of(getNx(matcher, "n1"), getNx(matcher, "n2"), getNx(matcher, "n3"), getNx(matcher, "n4"), getNx(matcher, "n5")
                    , getNx(matcher, "n6"), getNx(matcher, "n7"), getNx(matcher, "n8"), getNx(matcher, "n9"), getNx(matcher, "n10")
            );
            var our = List.of(
                    getNx(matcher, "n11"), getNx(matcher, "n12"), getNx(matcher, "n13"), getNx(matcher, "n14"), getNx(matcher, "n15"), getNx(matcher, "n16"), getNx(matcher, "n17"), getNx(matcher, "n18")
                    , getNx(matcher, "n19"), getNx(matcher, "n20"),
                    getNx(matcher, "n21"), getNx(matcher, "n22"), getNx(matcher, "n23"), getNx(matcher, "n24"), getNx(matcher, "n25"), getNx(matcher, "n26"), getNx(matcher, "n27"), getNx(matcher, "n28"), getNx(matcher, "n29"), getNx(matcher, "n30"),
                    getNx(matcher, "n31"), getNx(matcher, "n32"), getNx(matcher, "n33"), getNx(matcher, "n34"), getNx(matcher, "n35")
            );

            return new Card(number, winning, our);
        } else {
            throw new IllegalArgumentException("Bad input "+input);
        }
    }

    private static int getNx(Matcher matcher, String n) {
        return Integer.parseInt(matcher.group(n));
    }

    record Card(Integer cardNumber, List<Integer> winningNumbers, List<Integer> ourNumbers) {

        public int calculatePoints() {
            var count = getMatchingNumbers();
            var points = List.of(0,1,2,4,8,16,32,64,128,256,512,1024);
            return points.get(count);
        }

        public int getMatchingNumbers() {
            return (int) ourNumbers().stream().filter(winningNumbers::contains).count();
        }
    }
}