package be.johanbas.y2022.day06;

import be.johanbas.tools.Filez;

import java.util.HashSet;

public class Day06 {

//    public static int size = 4;
    public static int size = 14;

    public static void main(String[] args) {
        var challenge = Filez.readFile("input.txt", Day06.class).get(0);

        var start = 0;
        var substring = challenge.substring(start, start + size);
        while (!isMarker(substring)) {
            start++;
            substring = challenge.substring(start, start + size);
        }

        var split = challenge.split(substring);
        System.out.println(split[0].length()+size);
    }

    private static boolean isMarker(String substring) {
        var set = new HashSet<Character>();
        var chars = substring.toCharArray();
        for (var a : chars) {
            set.add(a);
        }
        return set.size() == size;
    }
}
