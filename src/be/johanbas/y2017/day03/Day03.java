package be.johanbas.y2017.day03;

/**
 * Created by Johan Bas on 3/12/2017.
 */
public class Day03 {

	public static void main(String[] args) {

		part2();

	}

	private static void part2() {
		int m = 501;
		int[][] matrix = new int[m][m];
		int mid = ((m-1)/2);
		System.out.println("mid = " + mid);
		int x = mid;
		int y = mid;

		boolean right = true;
		boolean down = false;
		boolean left= false;
		boolean up = false;

		matrix[mid][mid] = 1;

		int count = 0;
		int end = 347991;

		int sum = 0;
		while(sum<end) {

			count++;

			if(up) {
				x++;
				if(matrix[x][y+1] == 0) {
					up = false;
					right = true;
				}
			} else if(right) {
				y++;
				if(matrix[x-1][y] == 0) {
					right = false;
					left = true;
				}
			} else if(left) {
				x--;
				if(matrix[x][y-1] == 0) {
					left = false;
					down = true;
				}
			} else if(down) {
				y--;
				if(matrix[x+1][y] == 0) {
					down = false;
					up = true;
				}
			}

			sum = matrix[x+1][y] + matrix[x+1][y+1] + matrix[x+1][y-1] + matrix[x][y-1] + matrix[x][y+1]+ matrix[x-1][y+1]+matrix[x-1][y-1]+matrix[x-1][y];

			matrix[x][y] = sum;
		}

		System.out.println(sum);
	}

	private static void part1() {
		int m = 1001;
		int[][] matrix = new int[m][m];
		int mid = ((m-1)/2)+1;
		int x = mid;
		int y = mid;
		int count = 1;

		boolean right = true;
		boolean down = false;
		boolean left= false;
		boolean up = false;

		int end = 347991;

		while(true) {

			matrix[x][y] = count;
			if(count == end) {
//				printMatrix(matrix);
				System.out.println((x-mid) + " + " + (y-mid) + " = " + (Math.abs(x-mid)+Math.abs(y-mid)));
				break;
			}

			if(up) {
				x++;
				if(matrix[x][y+1] == 0) {
					up = false;
					right = true;
				}
			} else if(right) {
				y++;
				if(matrix[x-1][y] == 0) {
					right = false;
					left = true;
				}
			} else if(left) {
				x--;
				if(matrix[x][y-1] == 0) {
					left = false;
					down = true;
				}
			} else if(down) {
				y--;
				if(matrix[x+1][y] == 0) {
					down = false;
					up = true;
				}
			}


			count++;

		}
	}

	private static void printMatrix(int[][] matrix) {
		for (int[] ints : matrix) {
			for (int anInt : ints) {
				System.out.print(anInt + " ");
			}
			System.out.println();
		}
		System.out.println();
	}
}
