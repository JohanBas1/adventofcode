package be.johanbas.y2018.day03;

public class Claim {

    public int id;
    public int left;
    public int top;
    public int width;
    public int height;


    public Claim(String input) {
        // #1283 @ 646,264: 20x25
        id = Integer.parseInt(input.split(" @ ")[0].substring(1));

        String[] split = input.split(" @ ")[1].split(": ");

        left = Integer.parseInt(split[0].split(",")[0]);
        top = Integer.parseInt(split[0].split(",")[1]);

        width = Integer.parseInt(split[1].split("x")[0]);
        height = Integer.parseInt(split[1].split("x")[1]);
    }
}
