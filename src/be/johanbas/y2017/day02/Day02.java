package be.johanbas.y2017.day02;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Johan Bas on 2/12/2017.
 */
public class Day02 {

	public static void main(String[] args) throws IOException, URISyntaxException {

		List<String> input = readInput();
		List<Set<Integer>> inputs = new LinkedList<>();

		for (String s : input) {
			Set<Integer> lijst = new TreeSet<>();
			for (String s1 : s.split(",")) {
				lijst.add(Integer.parseInt(s1));
			}

			inputs.add(lijst);
		}

		int sum = 0;
		for (Set<Integer> integers : inputs) {
			Optional<Integer> min = integers.stream().min(Integer::compareTo);
			Optional<Integer> max = integers.stream().max(Integer::compareTo);
			sum += max.get();
			sum -= min.get();
		}

		int sum2 = 0;
		for (Set<Integer> integers : inputs) {
			for (Integer deler : integers) {
				for (Integer target : integers) {
					if(deler < target) {
						if(target % deler == 0) {
							sum2 += (target / deler);
						}
					}
				}
			}
		}

		System.out.println(sum);
		System.out.println(sum2);
	}


	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day02.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}
