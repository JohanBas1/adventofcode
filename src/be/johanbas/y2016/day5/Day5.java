package be.johanbas.day5;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Johan Bas on 5/12/2016.
 */
public class Day5 {

	public static void main(String[] args) throws NoSuchAlgorithmException {

		String door = "ojvtpuvg";
		int index = 1;
		int count = 0;
		String result = "";

//		System.out.println("PART 1");
//		System.out.println(part1(door, index, count));

		System.out.println("PART 2");
		System.out.println(part2(door, index, count));
	}

	private static String part1(String door, int index, int count) throws NoSuchAlgorithmException {

		String result = "";
		while (count < 8) {
			String md5 = md5(door+index);
			if(md5.startsWith("00000")) {
				System.out.println(index + " - " + md5 + " - " + md5.charAt(5));
				result += md5.charAt(5);
				count++;
			}
			index++;
		}
		return result;
	}

	private static char[] part2(String door, int index, int count) throws NoSuchAlgorithmException {

		char[] result = new char[8];
		while (count < 30) {
			String md5 = md5(door+index);
			if(md5.startsWith("00000")) {
				System.out.println(index + " - " + md5 + " - " + md5.charAt(5) + " " + md5.charAt(6));
//				result[((int) md5.charAt(5))] = md5.charAt(6);
				count++;
			}
			index++;
		}
		return result;
	}

	private static String md5(String plaintext) throws NoSuchAlgorithmException {
		MessageDigest m = MessageDigest.getInstance("MD5");
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
        // Now we need to zero pad it if you actually want the full 32 chars.
		while(hashtext.length() < 32 ){
			hashtext = "0"+hashtext;
		}

		return hashtext;
	}

}
