package be.johanbas.y2015.day17;

import java.util.*;

/*
--- Day 17: No Such Thing as Too Much ---

The elves bought too much eggnog again - 150 liters this time.
To fit it all into your refrigerator, you'll need to move it into smaller containers.
You take an inventory of the capacities of the available containers.

For example, suppose you have containers of size 20, 15, 10, 5, and 5 liters. If you need to store 25 liters, there are four ways to do it:

15 and 10
20 and 5 (the first 5)
20 and 5 (the second 5)
15, 5, and 5
Filling all containers entirely, how many different combinations of containers can exactly fit all 150 liters of eggnog?


--- Part Two ---

While playing with all the containers in the kitchen, another load of eggnog arrives!
The shipping and receiving department is requesting as many containers as you can spare.

Find the minimum number of containers that can exactly fit all 150 liters of eggnog.
How many different ways can you fill that number of containers and still hold exactly 150 litres?

In the example above, the minimum number of containers was two.
There were three ways to use that many containers, and so the answer there would be 3.

input 33,14,18,20,45,35,16,35,1,13,18,13,50,44,48,6,24,41,30,42
 */
public class Day17 {

    public static void main(String[] args) {
        List<Integer> containers = new LinkedList<>();
        containers.addAll(Arrays.asList(33, 14, 18, 20, 45, 35, 16, 35, 1, 13, 18, 13, 50, 44, 48, 6, 24, 41, 30, 42));

        Collections.sort(containers);
        System.out.println("Input " + containers);

        System.out.println("#powersets " + powerset(containers).size());
        System.out.println("answer1 " + powerset(containers).stream().filter(s -> s.stream().mapToInt(Integer::intValue).sum() == 150).count());

        System.out.println("#elements in shortest combi " + powerset(containers).stream().filter(s -> s.stream().mapToInt(Integer::intValue).sum() == 150).mapToInt(s -> s.size()).min().getAsInt()); // 4
        System.out.println("#of combi with 4 jugs " + powerset(containers).stream().filter(s -> s.size() == 4).filter(s -> s.stream().mapToInt(Integer::intValue).sum() == 150).count());
    }

    public static <T> List<List<T>> powerset(Collection<T> list) {
        List<List<T>> ps = new ArrayList<List<T>>();
        ps.add(new ArrayList<T>());   // add the empty set

        // for every item in the original list
        for (T item : list) {
            List<List<T>> newPs = new ArrayList<List<T>>();

            for (List<T> subset : ps) {
                // copy all of the current powerset's subsets
                newPs.add(subset);

                // plus the subsets appended with the current item
                List<T> newSubset = new ArrayList<T>(subset);
                newSubset.add(item);
                newPs.add(newSubset);
            }

            // powerset is now powerset of list.subList(0, list.indexOf(item)+1)
            ps = newPs;
        }
        return ps;
    }

}
