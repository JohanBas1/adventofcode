package be.johanbas.y2020.day5;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Day05 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        final List<String> strings = readInput();
//        System.out.println("Max = " + strings.stream().map(Seat::new).map(Seat::seatId).max(Integer::compareTo));

        final Set<Integer> seats = strings.stream().map(Seat::new).map(Seat::seatId).collect(Collectors.toSet());
        System.out.println("seats = " + seats);

        int start = seats.stream().findFirst().orElse(0) -1;
        for (Integer seat : seats) {
            if(start+1 == seat) {
                start = seat;
            } else {
                System.out.println("seat = " + (seat-1));
                break;
            }
        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day05.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
