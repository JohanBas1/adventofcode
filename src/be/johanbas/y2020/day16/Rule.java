package be.johanbas.y2020.day16;

import java.util.LinkedList;
import java.util.List;

public class Rule {

    // class: 1-3 or 5-7
    private String ruleName;

    private Integer start1;
    private Integer end1;

    private Integer start2;
    private Integer end2;

    List<Integer> matchesPaces = new LinkedList<>();

    public Rule(String ruleName, String start1, String end1, String start2, String end2) {
        this.ruleName = ruleName;
        this.start1 = Integer.parseInt(start1);
        this.end1 = Integer.parseInt(end1);
        this.start2 = Integer.parseInt(start2);
        this.end2 = Integer.parseInt(end2);
    }

    public boolean validateTicketValue(Integer ticketValue) {
        return (ticketValue >= start1 && ticketValue <= end1) || (ticketValue >= start2 && ticketValue <= end2);
    }

    public String getRuleName() {
        return ruleName;
    }

    public List<Integer> getMatchesPaces() {
        return matchesPaces;
    }

    public void setMatchesPaces(List<Integer> matchesPaces) {
        this.matchesPaces = matchesPaces;
    }
}
