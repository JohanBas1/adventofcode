package be.johanbas.y2015.day15;

import java.util.List;

public class Property {

    List<Integer> ingredients;

    public Property(List<Integer> ingredients) {
        this.ingredients = ingredients;
    }

    public int multiply(List<Integer> m) {
        int sum = 0;

        for (int i = 0; i < m.size(); i++) {
            sum += ingredients.get(i) * m.get(i);
        }

        if(sum < 0) {
            return 0;
        }
        return sum;
    }
}
