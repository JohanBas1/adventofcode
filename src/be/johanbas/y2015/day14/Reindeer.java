package be.johanbas.y2015.day14;

/**
 * Created by Johan Bas on 18/12/2015.
 */
public class Reindeer {

//    Vixen can fly 19 km/s for 7 seconds, but then must rest for 124 seconds.

    private final int speed;
    private final int raceTime;
    private final int restTime;
    private final String name;
    private int score = 0;

    public Reindeer(int speed, int raceTime, int restTime, String name) {
        this.speed = speed;
        this.raceTime = raceTime;
        this.restTime = restTime;
        this.name = name;
    }

    public int run(int seconds) {
        int distance = 0;

        distance += speed * (seconds / (raceTime+restTime)) * raceTime;

        int rest = seconds % (raceTime+restTime);

        if(rest > raceTime) {
            distance += raceTime * speed;
        } else {
            distance += rest * speed;
        }

        return distance;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void win() {
        score++;
    }
}
