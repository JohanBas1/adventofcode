package be.johanbas.y2020.day16;

import java.util.LinkedList;
import java.util.List;

public class Ticket {

    List<Integer> ticketValues = new LinkedList<>();

    public Ticket(String[] split) {
        for (String s : split) {
            ticketValues.add(Integer.parseInt(s));
        }
    }
}
