package be.johanbas.y2020.day2;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Password {

    private final int countMin;
    private final int countMax;
    private final char letter;
    private final String password;

    public Password(String input) {
        Pattern pattern = Pattern.compile("(\\d+)-(\\d+) (\\w): (\\w+)");

        final Matcher matcher = pattern.matcher(input);
        matcher.matches();
        countMin = Integer.parseInt(matcher.group(1));
        countMax = Integer.parseInt(matcher.group(2));
        letter = matcher.group(3).charAt(0);
        password = matcher.group(4);
    }

    public boolean valid() {
        int count = 0;
        for (char c : password.toCharArray()) {
            if(c == letter) {
                count++;
            }
        }

        return (count >= countMin && count <= countMax);
    }

    public boolean valid2() {
        final char[] chars = password.toCharArray();

        if(chars[countMin-1] == letter && chars[countMax-1] == letter) {
            return false;
        }

        return (chars[countMin-1] == letter || chars[countMax-1] == letter);
    }
}
