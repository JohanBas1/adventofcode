package be.johanbas.y2020.day11;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

public class Day11 {


    public static void main(String[] args) throws IOException, URISyntaxException {

        final List<String> input = readInput();

        char[][] from = new char[input.size()][input.get(0).length()];
        char[][] to = new char[input.size()][input.get(0).length()];

        for (int i = 0; i < input.size(); i++) {
            final char[] chars = input.get(i).toCharArray();
            for (int j = 0; j < chars.length; j++) {
                from[i][j] = chars[j];
            }
        }

        run(input, from, to);


    }

    private static List<Character> getAdjacentSeatsPart2Of(char[][] from, int i, int j) {
        List<Character> adjacent = new LinkedList<>();

        addFirstAdjacent(from, i, (x) -> x-1, j, (y) -> y-1, adjacent);
        addFirstAdjacent(from, i, (x) -> x-1, j, (y) -> y, adjacent);
        addFirstAdjacent(from, i, (x) -> x-1, j, (y) -> y+1, adjacent);
        addFirstAdjacent(from, i, (x) -> x, j, (y) -> y-1, adjacent);
        addFirstAdjacent(from, i, (x) -> x, j, (y) -> y+1, adjacent);
        addFirstAdjacent(from, i, (x) -> x+1, j, (y) -> y-1, adjacent);
        addFirstAdjacent(from, i, (x) -> x+1, j, (y) -> y, adjacent);
        addFirstAdjacent(from, i, (x) -> x+1, j, (y) -> y+1, adjacent);

        return adjacent;
    }

    private static void addFirstAdjacent(char[][] from, int i, final Function<Integer, Integer> fi, int j, final Function<Integer, Integer> fj, List<Character> adjacent) {
        i = fi.apply(i);
        j = fj.apply(j);
        if(i >= 0 && j>= 0 && i <= from.length-1 && j <= from[i].length-1) {
            final char c = from[i][j];
            if(c != '.') {
                adjacent.add(c);
            } else {
                addFirstAdjacent(from, i, fi, j, fj, adjacent);
            }
        }

    }

    private static void run(List<String> input, char[][] from, char[][] to) {
        checkSeats(from, to);
        while(!isMatrixSame(from, to)) {
            from = to;
            to = new char[input.size()][input.get(0).length()];
            checkSeats(from, to);
        }

        countOccupiedSeats(from);
    }

    private static void countOccupiedSeats(char[][] from) {
        int count = 0;
        for (char[] chars : from) {
            for (char aChar : chars) {
                if(aChar == '#') {
                    count++;
                }
            }
        }
        System.out.println("count = " + count);
    }

    private static boolean isMatrixSame(char[][] from, char[][] to) {
        for (int i = 0; i < from.length; i++) {
            for (int j = 0; j < from[i].length; j++) {
                if(from[i][j] != to[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    private static void checkSeats(char[][] from, char[][] to) {

        for (int i = 0; i < from.length; i++) {
            for (int j = 0; j < from[i].length; j++) {

                if(from[i][j] == '.') {
                    to[i][j] = '.';
                } else {

//                    List<Character> adjacentSeats = getAdjacentSeatsOf(from, i, j);
//                    int maxOccupied = 3;
                    List<Character> adjacentSeats = getAdjacentSeatsPart2Of(from, i, j);
                    int maxOccupied = 4;

                    if(from[i][j] == 'L') {

//                        List<Character> adjacentSeats = getAdjacentSeatsPart2Of(from, i, j);

                        if(!adjacentSeats.contains('#')) {
                            to[i][j] = '#';
                        } else {
                            to[i][j] = 'L';
                        }

                    } else {

                        if(adjacentSeats.stream().filter(character -> character.equals('#')).count() > maxOccupied) {
                            to[i][j] = 'L';
                        } else {
                            to[i][j] = '#';
                        }
                    }
                }
            }
        }
    }

    private static List<Character> getAdjacentSeatsOf(char[][] from, int i, int j) {
        List<Character> adjacent = new LinkedList<>();

        tryGetFromMatrix(i - 1,j - 1, adjacent, from);
        tryGetFromMatrix(i - 1,j, adjacent, from);
        tryGetFromMatrix(i - 1,j + 1, adjacent, from);
        tryGetFromMatrix(i,j - 1, adjacent, from);
        tryGetFromMatrix(i,j + 1, adjacent, from);
        tryGetFromMatrix(i + 1,j - 1, adjacent, from);
        tryGetFromMatrix(i + 1,j, adjacent, from);
        tryGetFromMatrix(i + 1,j + 1, adjacent, from);

        return adjacent;
    }

    private static void tryGetFromMatrix(int i, int j, List<Character> adjacent, char[][] from) {
        try {
            adjacent.add(from[i][j]);
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day11.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
