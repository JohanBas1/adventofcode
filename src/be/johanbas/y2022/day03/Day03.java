package be.johanbas.y2022.day03;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

public class Day03 {

    private static String table = " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static void main(String[] args) throws URISyntaxException, IOException {

        var part1 = readInput().stream().map(Backpack::new)
                .map(Backpack::getDouble)
                .map(x -> table.indexOf(x))
                .reduce(Integer::sum);

        System.out.println("part1 = " + part1);

        var part2 = readInput().stream().map(Backpack::new).toList();

        var badges = new LinkedList<Character>();
        for(var i=0; i<part2.size(); i=i+3) {
            var a = part2.get(i).contents();
            var b = part2.get(i+1).contents();
            var c = part2.get(i+2).contents();

            for (var x : a.toCharArray()) {
                if(b.contains(x+"") && c.contains(x+"")) {
                    // badge found
                    badges.add(x);
                    break;
                }
            }
        }

        System.out.println(badges);

        var p2 = badges.stream().map(x -> table.indexOf(x))
                .reduce(Integer::sum);

        System.out.println("part2 = " + p2);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        var input = Day03.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}

record Backpack(String contents) {

    public char getDouble() {
        var compartmentSize = contents.length() / 2;

        var first = contents.substring(0, compartmentSize);
        var second = contents.replace(first,"");

        for (var c : first.toCharArray()) {
            if(second.contains(c+"")) {
                return c;
            }
        }

        return ' ';
    }
}