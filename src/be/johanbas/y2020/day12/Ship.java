package be.johanbas.y2020.day12;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ship {

    private int x = 0;
    private int y = 0;
    private int compass = 90;

    private int waypointX = 10;
    private int waypointY = 1;

    public void sail(String direction) {

        Matcher matcher = Pattern.compile("(.)(\\d+)").matcher(direction);
        if (matcher.matches()) {
            final String instruction = matcher.group(1);
            final int value = Integer.parseInt(matcher.group(2));

            switch (instruction) {
                case "N":
                    y += value;
                    break;
                case "E":
                    x += value;
                    break;
                case "S":
                    y -= value;
                    break;
                case "W":
                    x -= value;
                    break;
                case "L":
                    compass -= value;
                    if(compass < 0)
                        compass += 360;
                    break;
                case "R":
                    compass += value;
                    compass = compass % 360;
                    break;
                case "F":
                    if(compass == 90) {
                        x += value;
                    } else if (compass == 180) {
                        y -= value;
                    } else if(compass == 270) {
                        x -= value;
                    } else {
                        y += value;
                    }
                    break;
            }
        } else {
            System.out.println("could not parse = " + direction);
        }
//        System.out.print(direction + " - ");
//        getPosition();
    }

    public void sailToWaypoint(String direction) {
        Matcher matcher = Pattern.compile("(.)(\\d+)").matcher(direction);
        if (matcher.matches()) {
            final String instruction = matcher.group(1);
            int value = Integer.parseInt(matcher.group(2));

            switch (instruction) {
                case "N":
                    waypointY += value;
                    break;
                case "E":
                    waypointX += value;
                    break;
                case "S":
                    waypointY -= value;
                    break;
                case "W":
                    waypointX -= value;
                    break;
                case "L":

                    while(value > 0) {
                        final int[] ints = rotatePoint(waypointX, waypointY, false);
                        waypointX = ints[0];
                        waypointY = ints[1];
                        value -= 90;
                    }

                    break;
                case "R":

                    while(value > 0) {
                        final int[] ints = rotatePoint(waypointX, waypointY, true);
                        waypointX = ints[0];
                        waypointY = ints[1];
                        value -= 90;
                    }

                    break;
                case "F":

                    x += (waypointX * value);
                    y += (waypointY * value);

                    break;
            }
        } else {
            System.out.println("could not parse = " + direction);
        }

//        System.out.print(direction + " - ");
//        getPosition();
    }

    public static int[] rotatePoint(int x, int y, boolean right) {
        return right ? new int[]{y, -x} : new int[]{-y, x};
    }



    public void getPosition() {
//        System.out.println("x = " + x + ", y = " + y + " waypointx:" + waypointX + "  waypointy:" + waypointY);

        System.out.println("x: " + Math.abs(x));
        System.out.println("y: " + Math.abs(y));
        System.out.println("sollution = " + (Math.abs(x) + Math.abs(y))); // 8131 is mis
    }
}
