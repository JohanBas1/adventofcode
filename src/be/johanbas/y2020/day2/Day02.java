package be.johanbas.y2020.day2;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day02 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        System.out.println(readInput().stream().map(Password::new).filter(Password::valid2).count());


    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day02.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
