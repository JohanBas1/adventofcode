package be.johanbas.y2017.day05;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Johan Bas on 7/12/2017.
 */
public class Node {

	String name;
	int weight;
	List<Node> children;
	Set<String> textChildren;
	Node parent;

	public Node(String name, int weight, Set<String> s) {
		this.name = name;
		this.weight = weight;
		children = new LinkedList<>();
		textChildren = s;
	}

	public boolean areChilderenBalenced() {
		if(children == null || children.size() == 0) {
			return true;
		}
		int weight = children.get(0).getWeight();
		for (Node child : children) {
			if(weight != child.getWeight()) {
				return false;
			}
		}

		return true;
	}

	public int getBalence() {
		int b = weight;
		for (Node child : children) {
			b += child.getBalence();
		}
		return b;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public List<Node> getChildren() {
		return children;
	}

	public void setChildren(List<Node> children) {
		this.children = children;
	}

	public Set<String> getTextChildren() {
		return textChildren;
	}

	public void setTextChildren(Set<String> textChildren) {
		this.textChildren = textChildren;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	@Override
	public String toString() {
		return "Node{" +
				"name='" + name + '\'' +
				", balence" + getBalence() +
				", weight=" + weight +
				", textChildren=" + textChildren +
				", parent=" + parent.getName() +
				'}';
	}
}
