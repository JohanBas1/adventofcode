package be.johanbas.y2022.day04;

import be.johanbas.tools.Filez;

public class Day04 {

    public static void main(String[] args) {
        var input = Filez.readFile("input.txt", Day04.class);

        var part1 = input.stream().map(Day04::parseAssignment).filter(Assignment::hasFullOverlap).count();
        System.out.println("part1 = " + part1);

        var part2 = input.stream().map(Day04::parseAssignment).filter(Assignment::hasAnyOverlap).count();
        System.out.println("part2 = " + part2);
    }

    public static Assignment parseAssignment(String i) {
        var split = i.split(",");
        var ab = split[0].split("-");
        var xy = split[1].split("-");
        return new Assignment(Integer.parseInt(ab[0]), Integer.parseInt(ab[1]), Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
    }
}

record Assignment(int a, int b, int x, int y) {

    public boolean hasFullOverlap() {
        if ((a <= x && b >= y) || (x <= a && y >= b)) {
//            System.out.println(a + "-" + b + "," + x + "-" + y);
            return true;
        } else {
            return false;
        }
    }

    public boolean hasAnyOverlap() { // 727 to low
        if ((a >= x && a <= y) ||
                (b >= x && b <= y) ||
                (x >= a && x <= b) ||
                (y >= a && y <= b)) {
//            System.out.println(a + "-" + b + "," + x + "-" + y);
            return true;
        } else {
            return false;
        }
    }
}
