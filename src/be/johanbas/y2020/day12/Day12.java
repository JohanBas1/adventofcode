package be.johanbas.y2020.day12;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day12 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        Ship boot = new Ship();

        final List<String> directions = readInput();
        for (String direction : directions) {
            boot.sailToWaypoint(direction);
        }

        boot.getPosition();
    }



    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day12.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}
