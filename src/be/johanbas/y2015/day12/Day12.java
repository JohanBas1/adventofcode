package be.johanbas.y2015.day12;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
--- Day 12: JSAbacusFramework.io ---

Santa's Accounting-Elves need help balancing the books after a recent order.
Unfortunately, their accounting software uses a peculiar storage format. That's where you come in.

They have a JSON document which contains a variety of things: arrays ([1,2,3]), objects ({"a":1, "b":2}),
numbers, and strings. Your first job is to simply find all of the numbers throughout the document and add them together.

For example:

[1,2,3] and {"a":2,"b":4} both have a sum of 6.
[[[3]]] and {"a":{"b":4},"c":-1} both have a sum of 3.
{"a":[-1,1]} and [-1,{"a":1}] both have a sum of 0.
[] and {} both have a sum of 0.
You will not encounter any strings containing numbers.

What is the sum of all numbers in the document?


--- Part Two ---

Uh oh - the Accounting-Elves have realized that they double-counted everything red.

Ignore any object (and all of its children) which has any property with the value "red". Do this only for objects ({...}), not arrays ([...]).

[1,2,3] still has a sum of 6.
[1,{"c":"red","b":2},3] now has a sum of 4, because the middle object is ignored.
{"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because the entire structure is ignored.
[1,"red",5] has a sum of 6, because "red" in an array has no effect.
 */
public class Day12 {

    public static Set<JsonNodeType> types = new HashSet<>();

    public static void main(String[] args) throws IOException, URISyntaxException {

        String content = new String(Files.readAllBytes(Paths.get(Day12.class.getResource("input.json").toURI())));

        sum(content);

        JsonNode node = new ObjectMapper().readTree(content);
        System.out.println(sumTree(node)); // 96852

    }

    private static void sum(String content) {
        StringBuilder number = new StringBuilder();
        int total = 0;
        for (char character : content.toCharArray()) {
            if (Character.isDigit(character) || character == '-') {
                number = number.append(character);
            } else {
                if (!number.toString().equals("")) {
                    total += Integer.parseInt(number.toString());
                    number = new StringBuilder();
                }
            }
        }
        System.out.println(total); // 156366
    }


    public static int sumTree(JsonNode node) {
        if(node.isInt()) {
            return node.asInt();
        }

        if(hasRed(node)) {
            return 0;
        }

        int sum = 0;
        for (JsonNode n : node) {
            sum += (sumTree(n));
        }

        return sum;
    }

    public static boolean hasRed(JsonNode node) {
        Iterator<JsonNode> iterator = node.iterator();

        if(!node.isObject()) {
            return false;
        }

        while(iterator.hasNext()) {
            if("red".equals(iterator.next().asText())) {
                return true;
            }
        }

        return false;
    }
}
