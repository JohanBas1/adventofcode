package be.johanbas.y2015.day21;

/**
 * Created by Johan Bas.
 */
public class Armor implements Item {

    private int armor;
    private int cost;
    private String name;

    public Armor(int cost, int armor, String name) {
        this.armor = armor;
        this.cost = cost;
        this.name = name;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    @Override
    public int getDmg() {
        return 0;
    }

    @Override
    public void setDmg(int dmg) {

    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Armor armor = (Armor) o;

        return name != null ? name.equals(armor.name) : armor.name == null;

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
