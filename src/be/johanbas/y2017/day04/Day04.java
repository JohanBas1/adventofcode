package be.johanbas.y2017.day04;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Johan Bas on 4/12/2017.
 */
public class Day04 {

	public static void main(String[] args) throws IOException, URISyntaxException {

		List<String> input = readInput();
		int count = 0;

		for (String s : input) {
			String[] split = s.split(" ");
			HashSet<String> tHashSet = new HashSet<>(Arrays.asList(split));
			if(split.length == tHashSet.size()) {
				count++;
			}
		}
		System.out.println("count = " + count);

		int count2 = 0;
		for (String s : input) {
			String[] split = s.split(" ");
			Set<String> sortedStrings = new TreeSet<>();

			for (String s1 : split) {
				char[] chars = s1.toCharArray();
				Arrays.sort(chars);
				sortedStrings.add(new String(chars));
			}

			if(split.length == sortedStrings.size()) {
				count2++;
			}
		}
		System.out.println("count2 = " + count2);
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day04.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}

}
