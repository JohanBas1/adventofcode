package be.johanbas.y2018.day03;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day03 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        List<String> strings = readInput();

//        part1(strings);
        part2(strings);

    }


    private static void part2(List<String> strings) {
        int[][] fabric = new int[2000][2000];

        Set<Integer> invalidIds = new HashSet<>();

        for (String string : strings) {
            Claim c = new Claim(string);

            for (int h = c.top; h < c.height + c.top; h++) {
                for (int w = c.left; w < c.width + c.left; w++) {
                    if(fabric[h][w] == 0) {
                        fabric[h][w] = c.id;
                    } else {
                        invalidIds.add(c.id);
                        invalidIds.add(fabric[h][w]);
                    }
                }
            }
        }

        for (String string : strings) {
            int id = new Claim(string).id;
            if(!invalidIds.contains(id)) {
                System.out.println("id = " + id);
                break;
            }
        }
    }

    private static void part1(List<String> strings) {
        int[][] fabric = new int[2000][2000];

        for (String string : strings) {
            Claim c = new Claim(string);

            for (int h = c.top; h < c.height + c.top; h++) {
                for (int w = c.left; w < c.width + c.left; w++) {
                    fabric[h][w] += 1;
                }
            }
        }

        int count = 0;
        for (int[] ints : fabric) {
            for (int anInt : ints) {
                if(anInt > 1) {
                    count += 1;
                }
            }
        }
        System.out.println("count = " + count);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day03.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
