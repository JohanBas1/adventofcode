package be.johanbas.y2020.day16;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day16 {

    static int sumPart1 = 0;
    static List<Rule> rules = new LinkedList<>();

    public static void main(String[] args) throws IOException, URISyntaxException {

        final List<String> list = readInput();
        List<Ticket> tickets = new LinkedList<>();

        for (String challenge : list) {

            Pattern rule = Pattern.compile("([a-z ]+): (\\d+)-(\\d+) or (\\d+)-(\\d+)");
            final Matcher matcher = rule.matcher(challenge);
            if (matcher.matches()) {
                rules.add(new Rule(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4), matcher.group(5)));
            } else if(challenge.contains(",")) {
                tickets.add(new Ticket(challenge.split(",")));
            }
        }

        tickets = tickets.subList(1,tickets.size());

        part1(tickets);
        part2(tickets, tickets.get(0));
    }

    private static void part1(List<Ticket> tickets) {
        for (Ticket ticket : tickets) {
            isTicketValid(ticket);
        }
        System.out.println("sumPart1 = " + sumPart1);
    }


    private static void part2(List<Ticket> tickets, Ticket myTicket) {
        final List<Ticket> validTickets = tickets.stream().filter(Day16::isTicketValid).collect(Collectors.toList());

        // map maken met alle eerste values, tweede values, etc en deze aan elke ruleValue geven en zien of elke value geldig is.

        Map<Integer,List<Integer>> map = new HashMap<>();
        for (Ticket validTicket : validTickets) {
            for (int i = 0; i < validTicket.ticketValues.size(); i++) {
                if(map.containsKey(i)) {
                    map.get(i).add(validTicket.ticketValues.get(i));
                } else {
                    List<Integer> l = new LinkedList<>();
                    l.add(validTicket.ticketValues.get(i));
                    map.put(i,l);
                }
            }
        }

        System.out.println("map = " + map);

        for (Map.Entry<Integer, List<Integer>> entry : map.entrySet()) {

//            final List<Rule> departure = rules.stream().filter(r -> r.getRuleName().contains("departure")).collect(Collectors.toList());
            for (Rule rule : rules) {
                if(entry.getValue().stream().allMatch(rule::validateTicketValue)) {
                    System.out.println("match at " + rule.getRuleName());
                    rule.matchesPaces.add(entry.getKey());
                }
            }
        }

        System.out.println("Done");
        for (Rule rule : rules) {
            System.out.println("ruleValue = " + rule.getRuleName() + " matches " + rule.matchesPaces);
        }

        /*
ruleValue = departure location matches [19]
ruleValue = departure time matches [16]
ruleValue = departure station matches [10]
ruleValue = departure platform matches [5]
ruleValue = departure track matches [11]
ruleValue = departure date matches [6]
         */

        BigInteger multiply = BigInteger.ONE;
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(19)+""));
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(16)+""));
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(10)+""));
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(5)+""));
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(11)+""));
        multiply = multiply.multiply(new BigInteger(myTicket.ticketValues.get(6)+""));
        System.out.println("multiply = " + multiply);
    }

    private static boolean isTicketValid(Ticket ticket) {
        for (Integer ticketValue : ticket.ticketValues) {
            if(rules.stream().noneMatch(rule -> rule.validateTicketValue(ticketValue))) {
                sumPart1 += ticketValue;
                return false;
            }
        }
        return true;
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day16.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}
