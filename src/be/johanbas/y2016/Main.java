package be.johanbas;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
	// write your code here

        MessageDigest crypt = MessageDigest.getInstance("SHA-1");
        crypt.reset();

        crypt.update("280eef37f36107b193ecb37be8daacb51e63b1d1".getBytes("UTF-8"));
        crypt.update("TtdaivdJB2017".getBytes("UTF-8"));

        byte[] digest = crypt.digest();
        final String s = new BigInteger(1, digest).toString(16);
        System.out.println(s);

    }
}
