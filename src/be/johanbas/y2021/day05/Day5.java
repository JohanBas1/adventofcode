package be.johanbas.y2021.day05;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day5 {

//    public static void main(String[] args) throws URISyntaxException, IOException {
//
//        var input = readInput();
//        var lines = new LinkedList<Line>();
//
//        var pattern = Pattern.compile("(\\d+),(\\d+) -> (\\d+),(\\d+)");
//        for (String line : input) {
//            Matcher matcher = pattern.matcher(line);
//            matcher.matches();
//
//            lines.add(new Line(matcher.group(1), matcher.group(2), matcher.group(3), matcher.group(4)));
//        }
//
////        var straightLines = lines.stream().filter(Line::isHorizontalOrVertical).collect(Collectors.toList());
////        calculateOverlap(straightLines);
//
//        calculateOverlap(lines);
//
//    }
//
//    private static void calculateOverlap(List<Line> straightLines) {
//        var points = new HashMap<Point, Integer>();
//        straightLines.forEach(line -> line.getAllPoints().forEach(point -> {
//            points.computeIfAbsent(point, count -> 0);
//            points.put(point, points.get(point)+1);
//        }));
//
//        final long count = points.values().stream().filter(integer -> integer > 1).count();
//        System.out.println("count = " + count);
//    }
//
//    private static List<String> readInput() throws URISyntaxException, IOException {
//        URI input = Day5.class.getResource("input.txt").toURI();
//        return Files.readAllLines(Paths.get(input));
//    }
}
