package be.johanbas.y2020.day18;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Expression {

    private String result = "";

    public Expression(String expression) {

        // haken opkuisen

        int balance = 0;
        int subexpressionStart = 0;

        final char[] chars = expression.toCharArray();
        for (int i = 0; i < chars.length; i++) {

            final char aChar = chars[i];

            if(aChar == '(') {

                if(balance == 0) {
                    subexpressionStart = i;
                }
                balance = balance+1;

            } else if(aChar == ')') {
                balance--;

                if(balance == 0) {
                    final String sub = expression.substring(subexpressionStart + 1, i);
                    Expression subExpression = new Expression(sub);
                    final BigInteger subResult = subExpression.eval();
                    result = result + subResult;
                }
            } else {
                if(balance == 0) {
                    result = result + aChar;
                }
            }

        }
    }

    // evaluate expression
    public BigInteger eval() {
        List<String> list = makeList();

//        return part1(list);

        return part2(list);
    }

    private BigInteger part2(List<String> list) {
        List<String> rest = new LinkedList<>();

        BigInteger prev = new BigInteger(list.get(0));
        for (int i = 1; i < list.size(); i=i+2) {
            if(list.get(i).equals("+")) {
                prev = prev.add(new BigInteger(list.get(i+1)));
            } else {
                rest.add(prev.toString());
                rest.add(list.get(i));
                prev = new BigInteger(list.get(i+1));

            }
        }
        rest.add(prev.toString());

        return rest.stream().filter(e -> !e.equals("*")).map(BigInteger::new).reduce(BigInteger::multiply).orElseThrow();

    }

    private BigInteger part1(List<String> list) {
        BigInteger start = new BigInteger(list.get(0));
        for (int i = 1; i < list.size(); i = i+2) {
            if(list.get(i).equals("+")) {
                start = start.add(new BigInteger(list.get(i+1)));
            } else {
                start = start.multiply(new BigInteger(list.get(i+1)));
            }
        }

//        System.out.println(result + " -> " + list + " -> " + start);
        return start;
    }

    private List<String> makeList() {
        Pattern pattern = Pattern.compile("(\\d+)([+*])?");

        final Matcher matcher = pattern.matcher(result);
        matcher.matches();

        List<String> list = new LinkedList<>();
        while(matcher.find()) {
            list.add(matcher.group(1));

            if(matcher.group(2) != null)
            list.add(matcher.group(2));
        }
        return list;
    }
}
