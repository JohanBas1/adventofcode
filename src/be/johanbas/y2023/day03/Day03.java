package be.johanbas.y2023.day03;

import be.johanbas.tools.Filez;

import java.math.BigInteger;
import java.util.*;

public class Day03 {

    public static void main(String[] args) {
        var input = Filez.readFile("input.txt", Day03.class);

        List<PartNumber> numbers = new LinkedList<>();

        for (var i = 0; i < input.size(); i++) {
            var row = input.get(i);

            var number = "";
            var symbol = new HashSet<SymbolPos>();
            for (var j = 0; j < row.toCharArray().length; j++) {

                if(Character.isDigit(row.charAt(j))) {
                    symbol.addAll(hasAdjacentSymbol(i, j, input));
                    number += row.charAt(j);
                } else {
                    if (!number.isBlank()) {
                        numbers.add(new PartNumber(Integer.parseInt(number), symbol));
                        number = "";
                        symbol = new HashSet<>();
                    }
                }
            }
            if (!number.isBlank()) {
                numbers.add(new PartNumber(Integer.parseInt(number), symbol));
            }
        }

        System.out.println("Part1: "+numbers.stream().filter(PartNumber::hasSymbol).map(PartNumber::number).reduce(Integer::sum).orElse(-1));

        var symbolMap = new HashMap<SymbolPos, Set<Integer>>();
        numbers.forEach(num -> {
            num.symbols().stream().forEach(e -> {
                symbolMap.computeIfAbsent(e, k -> new HashSet<>());
                symbolMap.get(e).add(num.number);
            });
        });

        var count = symbolMap.entrySet().stream()
                .filter(e -> e.getKey().symbol == '*')
                .filter(e -> e.getValue().size() == 2)
                .map(e -> e.getValue().stream().reduce((a,b) -> a*b).orElse(0))
                .reduce(Integer::sum).orElse(0);
        System.out.println("Part2: "+count);
    }

    record PartNumber(int number, HashSet<SymbolPos> symbols) {
        boolean hasSymbol() {
            return !symbols.isEmpty();
        }
    }

    private static List<SymbolPos> hasAdjacentSymbol(int i, int j, List<String> input) {
        List<SymbolPos> sList = new LinkedList<>();
        if(isSymbolAt(i-1,j-1,input).isPresent()) { sList.add(isSymbolAt(i-1,j-1,input).get()); }
        if(isSymbolAt(i-1,j,input).isPresent()) { sList.add(isSymbolAt(i-1,j,input).get()); }
        if(isSymbolAt(i-1,j+1,input).isPresent()) { sList.add(isSymbolAt(i-1,j+1,input).get()); }
        if(isSymbolAt(i,j-1,input).isPresent()) { sList.add(isSymbolAt(i,j-1,input).get()); }
        if(isSymbolAt(i,j+1,input).isPresent()) { sList.add(isSymbolAt(i,j+1,input).get()); }
        if(isSymbolAt(i+1,j-1,input).isPresent()) { sList.add(isSymbolAt(i+1,j-1,input).get()); }
        if(isSymbolAt(i+1,j,input).isPresent()) { sList.add(isSymbolAt(i+1,j,input).get()); }
        if(isSymbolAt(i+1,j+1,input).isPresent()) { sList.add(isSymbolAt(i+1,j+1,input).get()); }

        return sList;
    }

    private static Optional<SymbolPos> isSymbolAt(int i, int j, List<String> input) {
        if(i < 0 || i >= input.size()) return Optional.empty();
        if(j < 0 || j >= input.get(i).length()) return Optional.empty();

        var isSymbol = !String.valueOf(input.get(i).charAt(j)).matches("^[0-9.]");
        if(isSymbol) {
            return Optional.of(new SymbolPos(input.get(i).charAt(j), i, j));
        } else {
            return Optional.empty();
        }
    }

    record SymbolPos(char symbol, int x, int y) {}
}
