package be.johanbas.tools;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Filez {

    public static List<String> readFile(String file, Class clazz) {
        URI input = null;
        try {
            input = clazz.getResource(file).toURI();
            return Files.readAllLines(Paths.get(input));
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }



}
