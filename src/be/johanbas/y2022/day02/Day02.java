package be.johanbas.y2022.day02;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Day02 {

    public static void main(String[] args) throws URISyntaxException, IOException {

        var sum = readInput().stream()
                .map(s -> s.split(" "))
                .map(i -> new Round(i[0], i[1]))
                .map(Round::calculateScore)
                .reduce(Integer::sum);

        System.out.println("sum = " + sum);

        sum = readInput().stream()
                .map(s -> s.split(" "))
                .map(i -> new Round(i[0], i[1]))
                .map(Round::part2)
                .reduce(Integer::sum);

        System.out.println("sum = " + sum);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        var input = Day02.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}

record Round(String opponent, String me) {

    // The second column, you reason, must be what you should play in response: X for Rock, Y for Paper, and Z for Scissors
    // The score for a single round is the score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).

    public int calculateScore() {
        if(opponent.equals("A")) {
            // rock
            if(me.equals("X")) {
                // rock
                return 1 + 3;
            } else if(me.equals("Y")) {
                // paper
                return 2 + 6;
            } else {
                // Scissors
                return 3 + 0;
            }

        } else if(opponent.equals("B")) {
            // B for Paper
            if(me.equals("X")) {
                // rock
                return 1 + 0;
            } else if(me.equals("Y")) {
                // paper
                return 2 + 3;
            } else {
                // Scissors
                return 3 + 6;
            }

        } else {
            // C for Scissors
            if(me.equals("X")) {
                // rock
                return 1 + 6;
            } else if(me.equals("Y")) {
                // paper
                return 2 + 0;
            } else {
                // Scissors
                return 3 + 3;
            }
        }
    }

    public int part2() {
        if (opponent.equals("A")) {
            // rock
            if (me.equals("X")) {
                // lose
                return 3 + 0;
            } else if (me.equals("Y")) {
                // draw
                return 1 + 3;
            } else {
                // win
                return 2 + 6;
            }

        } else if (opponent.equals("B")) { //  (1 for Rock, 2 for Paper, and 3 for Scissors)
            // B for Paper
            if (me.equals("X")) {
                // lose
                return 1 + 0;
            } else if (me.equals("Y")) {
                // draw
                return 2 + 3;
            } else {
                // win
                return 3 + 6;
            }

        } else {
            // C for Scissors
            if (me.equals("X")) {
                // lose
                return 2 + 0;
            } else if (me.equals("Y")) {
                // draw
                return 3 + 3;
            } else {
                // win
                return 1 + 6;
            }
        }
    }
}
