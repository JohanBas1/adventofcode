package be.johanbas.y2015.day05;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**

--- Day 5: Doesn't He Have Intern-Elves For This? ---

Santa needs help figuring out which strings in his text file are naughty or nice.

A nice string is one with all of the following properties:

It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
For example:

ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
jchzalrnumimnmhp is naughty because it has no double letter.
haegwjzuvuyypxyu is naughty because it contains the string xy.
dvszwmarrgswjxmb is naughty because it contains only one vowel.
How many strings are nice?

--- Part Two ---

Realizing the error of his ways, Santa has switched to a better model of determining whether a string is naughty or nice. None of the old rules apply, as they are all clearly ridiculous.

Now, a nice string is one with all of the following properties:

It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
For example:

qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each ruleValue overlap.
uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.
How many strings are nice under these new rules?

 */
public class Day5 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        Stream<String> stream = readInput();
        System.out.println(stream.filter(x -> notContainsString(x)) // 912 ok
                .filter(x -> containsLetterTwice(x)) // 427
                .filter(x -> containsThreeVowels(x))
                .count());

        stream = readInput();
        System.out.println(stream.filter(x -> containsPair(x))
                .filter(x -> containsRepeat(x)).count());
    }

//    It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
    private static boolean containsRepeat(String x) {
        Pattern p = Pattern.compile("(.).\\1");
        return p.matcher(x).find();
    }

//    It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
    private static boolean containsPair(String x) {
        Pattern p = Pattern.compile("(..).*\\1");
        return p.matcher(x).find();
    }

//    It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
    private static boolean containsThreeVowels(String x) {
        return x.replaceAll("[^aeiou]","").length() > 2;
    }

//    It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
    private static boolean containsLetterTwice(String x) {
        char t = ' ';
        for (char c : x.toCharArray()) {
            if(c == t)
                return true;
            t = c;
        }
        return false;
    }

//    It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
    private static boolean notContainsString(String x) {
        return !(x.contains("ab") || x.contains("cd") || x.contains("pq") || x.contains("xy"));
    }

    private static Stream<String> readInput() throws URISyntaxException, IOException {
        URI input = Day5.class.getResource("input").toURI();
        return Files.lines(Paths.get(input));
    }
}
