package be.johanbas.y2020.day13;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Day13 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        final List<String> input = readInput();

//        part1();
        part2(input);
    }

    private static void part2(List<String> input) {
        final List<String> list = Arrays.asList(input.get(1).split(","));

        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            if(!list.get(i).equals("x")) {
                final int key = Integer.parseInt(list.get(i));
                map.put(key, key-i);
            }
        }

        System.out.println("Part2 = " + chineseReminder(List.copyOf(map.keySet()), List.copyOf(map.values())));
    }

    // op internet gevonden ...
    private static long chineseReminder(List<Integer> reminders, List<Integer> mod) {
        long product = reminders.stream()
                .mapToLong(i -> i)
                .reduce(1, (a, b) -> a * b);
        long sum = 0;

        for (int i = 0; i < reminders.size(); i++) {
            long partialProduct = product / reminders.get(i);
            long inverse = BigInteger.valueOf(partialProduct)
                    .modInverse(BigInteger.valueOf(reminders.get(i)))
                    .longValue();
            sum += partialProduct * inverse * mod.get(i);
        }

        return sum % product;
    }

    private static void part1() throws URISyntaxException, IOException {
        int arrivalTime = Integer.parseInt(readInput().get(0));
        System.out.println("my arrival time: " + arrivalTime);

        final List<Integer> departures = Arrays.stream(readInput().get(1).split(",")).filter(s -> !s.equals("x")).map(Integer::parseInt).collect(Collectors.toList());

        boolean found = false;
        int bus = 0;
        while(!found) {
            arrivalTime++;
            for (Integer departure : departures) {
                if (arrivalTime % departure == 0) {
                    bus = departure;
                    System.out.println("Bus " + departure);
                    System.out.println("At time: " + arrivalTime);
                    found = true;
                    break;
                }
            }

        }

        int waitTime = arrivalTime - Integer.parseInt(readInput().get(0));
        System.out.println("waitTime = " + waitTime);
        System.out.println("sollution:" + (bus * waitTime));
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day13.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
