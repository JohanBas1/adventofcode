package be.johanbas.y2015.day21;

import java.util.LinkedList;
import java.util.List;

/**
 --- Day 21: RPG Simulator 20XX ---

 Little Henry Case got a new video game for Christmas. It's an RPG, and he's stuck on a boss. He needs to know what equipment to buy at the shop. He hands you the controller.

 In this game, the player (you) and the enemy (the boss) take turns attacking. The player always goes first. Each attack reduces the opponent's hit points by at least 1.
 The first character at or below 0 hit points loses.

 Damage dealt by an attacker each turn is equal to the attacker's damage score minus the defender's armor score.
 An attacker always does at least 1 damage. So, if the attacker has a damage score of 8, and the defender has an armor score of 3, the defender loses 5 hit points.
 If the defender had an armor score of 300, the defender would still lose 1 hit point.

 Your damage score and armor score both start at zero.
 They can be increased by buying items in exchange for gold. You start with no items and have as much gold as you need.
 Your total damage or armor is equal to the sum of those stats from all of your items. You have 100 hit points.

 Here is what the item shop is selling:

 Weapons:    Cost  Damage  Armor
 Dagger        8     4       0          8
 Shortsword   10     5       0          2
 Warhammer    25     6       0          15
 Longsword    40     7       0          15
 Greataxe     74     8       0          34

 Armor:      Cost  Damage  Armor
 Leather      13     0       1          13
 Chainmail    31     0       2          18
 Splintmail   53     0       3          22
 Bandedmail   75     0       4          22
 Platemail   102     0       5          27

 Rings:      Cost  Damage  Armor
 Damage +1    25     1       0          25
 Damage +2    50     2       0          25
 Damage +3   100     3       0          50
 Defense +1   20     0       1          20
 Defense +2   40     0       2          20
 Defense +3   80     0       3          40


 You must buy exactly one weapon; no dual-wielding.
 Armor is optional, but you can't use more than one.
 You can buy 0-2 rings (at most one for each hand). You must use any items you buy.
 The shop only has one of each item, so you can't buy, for example, two rings of Damage +3.

 For example, suppose you have 8 hit points, 5 damage, and 5 armor, and that the boss has 12 hit points, 7 damage, and 2 armor:

 The player deals 5-2 = 3 damage; the boss goes down to 9 hit points.
 The boss deals 7-5 = 2 damage; the player goes down to 6 hit points.
 The player deals 5-2 = 3 damage; the boss goes down to 6 hit points.
 The boss deals 7-5 = 2 damage; the player goes down to 4 hit points.
 The player deals 5-2 = 3 damage; the boss goes down to 3 hit points.
 The boss deals 7-5 = 2 damage; the player goes down to 2 hit points.
 The player deals 5-2 = 3 damage; the boss goes down to 0 hit points.
 In this scenario, the player wins! (Barely.)

 You have 100 hit points. The boss's actual stats are in your puzzle input. What is the least amount of gold you can spend and still win the fight?


 Boss
 Hit Points: 104
 Damage: 8
 Armor: 1

 Turns out the shopkeeper is working with the boss, and can persuade you to buy whatever items he wants. The other rules still apply, and he still only has one of each item.

 What is the most amount of gold you can spend and still lose the fight?

 *
 */
public class Day21 {

    private static Integer minCost = Integer.MAX_VALUE;
    private static Integer maxCost = Integer.MIN_VALUE;

    public static void main(String[] args) {

        List<Weapon> weaponList = new LinkedList<>();
        weaponList.add(new Weapon(8,4,"Dagger"));
        weaponList.add(new Weapon(10,5,"Shortsword"));
        weaponList.add(new Weapon(25,6,"Warhammer"));
        weaponList.add(new Weapon(40,7,"Longsword"));
        weaponList.add(new Weapon(74,8,"Greataxe"));

        List<Armor> armors = new LinkedList<>();
        armors.add(new Armor(0,0,"ANone")); // optional
        armors.add(new Armor(13,1,"Leather"));
        armors.add(new Armor(31,2,"Chainmail"));
        armors.add(new Armor(53,3,"Splintmail"));
        armors.add(new Armor(75,4,"Bandedmail"));
        armors.add(new Armor(102,5,"Platemail"));

        List<Armor> armorRings = new LinkedList<>();
        armorRings.add(new Armor(0,0,"ARNone")); // optional
        armorRings.add(new Armor(0,0,"ARNone2")); // optional
        armorRings.add(new Armor(25,1,"defense +1"));
        armorRings.add(new Armor(50,2,"defense +2"));
        armorRings.add(new Armor(100,3,"defense +3"));

        List<Weapon> dmgRings = new LinkedList<>();
        dmgRings.add(new Weapon(0,0,"DRNone")); // optional
        dmgRings.add(new Weapon(0,0,"DRNone2")); // optional
        dmgRings.add(new Weapon(20,1,"attack +1"));
        dmgRings.add(new Weapon(40,2,"attack +2"));
        dmgRings.add(new Weapon(80,3,"attack +3"));


        bruteForceAllPossibilities(weaponList, armors, armorRings, dmgRings);

        System.out.println(fight(new Character(100, 6, 3), new Character(104, 8, 1)));
        System.out.println(fight(new Character(100, 5, 4), new Character(104, 8, 1)));
        System.out.println(fight(new Character(100, 4, 5), new Character(104, 8, 1)));

        System.out.println(minCost); // 78
        System.out.println(maxCost); // 148
    }

    private static void bruteForceAllPossibilities(List<Weapon> weaponList, List<Armor> armors, List<Armor> armorRings, List<Weapon> dmgRings) {
        for (Weapon weapon : weaponList) {
            for (Armor armor : armors) {
                for (Armor armorRing : armorRings) {
                    for (Armor secondArmorRing : armorRings) {
                        if(!secondArmorRing.equals(armorRing)) {
                            for (Weapon dmgRing : dmgRings) {
                                for (Weapon secondDmgRing : dmgRings) {
                                    if(!dmgRing.equals(secondDmgRing)) {

                                        int ringCount = countRings(armorRing, secondArmorRing, dmgRing, secondDmgRing);

                                        if(ringCount < 3) {

                                            Character me = new Character(100, (weapon.getDmg() + dmgRing.getDmg() + secondDmgRing.getDmg()), (armor.getArmor() + armorRing.getArmor() + secondArmorRing.getArmor()));
                                            int cost = weapon.getCost() + dmgRing.getCost() + secondDmgRing.getCost() + armor.getCost() + armorRing.getCost() + secondArmorRing.getCost();

                                            if (fight(me, new Character(104, 8, 1))) {
                                                if (cost < minCost) {
                                                    minCost = cost;
                                                }
                                            } else {
                                                if (cost > maxCost) {
                                                    print(weapon, armor, armorRing, secondArmorRing, dmgRing, secondDmgRing, cost);
                                                    maxCost = cost;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private static int countRings(Armor armorRing, Armor secondArmorRing, Weapon dmgRing, Weapon secondDmgRing) {
        int ringCount = 0;
        if(armorRing.getCost() > 0) {
            ringCount++;
        }
        if(secondArmorRing.getCost() > 0) {
            ringCount++;
        }
        if(dmgRing.getCost() > 0) {
            ringCount++;
        }
        if(secondDmgRing.getCost() > 0) {
            ringCount++;
        }
        return ringCount;
    }

    private static void print(Weapon weapon, Armor armor, Armor armorRing, Armor secondArmorRing, Weapon dmgRing, Weapon secondDmgRing, int cost) {
        System.out.println("----");
        System.out.println(weapon.getDmg()+" "+armor.getArmor()+" "+armorRing.getArmor()+" "+secondArmorRing.getArmor()+" "+dmgRing.getDmg()+" "+secondDmgRing.getDmg());
        System.out.println(weapon.getCost()+" "+armor.getCost()+" "+armorRing.getCost()+" "+secondArmorRing.getCost()+" "+dmgRing.getCost()+" "+secondDmgRing.getCost());
        System.out.println("cost " + cost);
    }

    private static boolean fight(Character me, Character boss) {
        while(me.alive() && boss.alive()) {
            // fight
            me.attack(boss);
            if(boss.alive()) {
                boss.attack(me);
            }
        }

        return me.alive();
    }
}

