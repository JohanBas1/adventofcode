package be.johanbas.y2020.day10;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Day10 {

    public static void main(String[] args) throws IOException, URISyntaxException {

//        subArrangement();

        final List<Integer> input = readInput();
        input.add(0);
        final Integer max = input.stream().max(Integer::compareTo).orElseThrow();
        input.add(max+3);

        Collections.sort(input);

//        System.out.println("input = " + input);

        Map<Integer, BigInteger> map = new TreeMap<>();
        input.forEach(integer -> map.put(integer,BigInteger.ZERO));
        map.put(0, BigInteger.ONE);

        for (Integer integer : input) {

            if(map.containsKey(integer+1)) {
                map.put(integer+1, map.get(integer+1).add(map.get(integer)));
            }

            if(map.containsKey(integer+2)) {
                map.put(integer+2, map.get(integer+2).add(map.get(integer)));
            }

            if(map.containsKey(integer+3)) {
                map.put(integer+3, map.get(integer+3).add(map.get(integer)));
            }
        }

//        System.out.println("map = " + map);
        System.out.println("result = " + map.get(max + 3));


    }

    private static void subArrangement() throws URISyntaxException, IOException {
        int een = 0;
        int drie = 1;

        var pref = 0;
        for (Integer integer : readInput()) {
            var temp = integer - pref;
            if(temp == 1) {
                een++;
            } else if(temp == 3) {
                drie++;
            }
            pref = integer;
        }

        System.out.println("een = " + een);
        System.out.println("drie = " + drie);

        System.out.println("part1: "+ (een * drie));
    }

    private static List<Integer> readInput() throws URISyntaxException, IOException {
        URI input = Day10.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input)).stream().map(Integer::parseInt).sorted().collect(Collectors.toList());
    }
}
