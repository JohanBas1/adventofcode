package be.johanbas.day1;

import be.johanbas.Pair;

import java.util.HashSet;
import java.util.Set;

public class Day1 {

	private final static String INPUT = "R4, R3, R5, L3, L5, R2, L2, R5, L2, R5, R5, R5, R1, R3, L2, L2, L1, R5, L3, R1, L2, R1, L3, L5, L1, R3, L4, R2, R4, L3, L1, R4, L4, R3, L5, L3, R188, R4, L1, R48, L5, R4, R71, R3, L2, R188, L3, R2, L3, R3, L5, L1, R1, L2, L4, L2, R5, L3, R3, R3, R4, L3, L4, R5, L4, L4, R3, R4, L4, R1, L3, L1, L1, R4, R1, L4, R1, L1, L3, R2, L2, R2, L1, R5, R3, R4, L5, R2, R5, L5, R1, R2, L1, L3, R3, R1, R3, L4, R4, L4, L1, R1, L2, L2, L4, R1, L3, R4, L2, R3, L1, L5, R4, R5, R2, R5, R1, R5, R1, R3, L3, L2, L2, L5, R2, L2, R5, R5, L2, R3, L5, R5, L2, R4, R2, L1, R3, L5, R3, R2, R5, L1, R3, L2, R2, R1";

	public static void main(String[] args) {

		int x = 0;
		int y = 0;
		int direction = 1; // up = 1, right = 2, down = 3, left = 4;

		boolean part2 = false;

		Set<Pair> previousPositions = new HashSet<>();

		for (String s : INPUT.split(", ")) {
			if(s.startsWith("R")) {
				direction += 1;
				if(direction > 4) direction -= 4;
			} else {
				direction -= 1;
				if(direction < 1) direction += 4;
			}

			final int steps = Integer.parseInt(s.replaceFirst("R", "").replaceFirst("L", ""));

			if(!part2) {
				for (int i = 0; i < steps; i++) {

					Pair p;

					if (direction == 1) {
						p = new Pair(x, y + i);
					} else if (direction == 2) {
						p = new Pair(x + i, y);
					} else if (direction == 3) {
						p = new Pair(x, y - i);
					} else {
						p = new Pair(x - i, y);
					}

					if (previousPositions.contains(p)) {
						System.out.println("Part 2:");
						System.out.println(Math.abs((Integer) p.getLeft()) + Math.abs((Integer) p.getRight()));
						part2 = true;
					} else {
						previousPositions.add(p);
					}
				}
			}

			if(direction == 1) {
				y += steps;
			} else if(direction == 2) {
				x += steps;
			} else if (direction == 3) {
				y -= steps;
			} else {
				x -= steps;
			}
		}

		System.out.println("Part 1:");
		System.out.println(Math.abs(x)+Math.abs(y));
	}
}