package be.johanbas.y2017.day06;

import java.util.*;

/**
 * Created by Johan Bas on 6/12/2017.
 */
public class Day06 {

	public static void main(String[] args) {

		long time = System.currentTimeMillis();

		int[] input = {0,5,10,0,11,14,13,4,11,8,8,7,1,4,12,11};
//		int[] input = {0,2,7,0};
		List<String> history = new LinkedList<>();

		int[] integers = input;
		while(!history.contains(convert(integers))) {
			history.add(convert(integers));
			integers = reDistribute(integers.clone());
		}

		System.out.println(history.size());
		String convert = convert(integers);
		for (int i = 0; i < history.size(); i++) {
			if(history.get(i).equals(convert)) {
				System.out.println((history.size() - i));
			}
		}

		time = (System.currentTimeMillis() - time);
		System.out.println("time = " + time);
	}

	private static int[] reDistribute(int[] input) {

		int max = 0;
		int pos = -1;
		for (int i = 0; i < input.length; i++) {
			if(input[i] > max) {
				pos = i;
				max = input[i];
			}
		}
		input[pos] = 0;

		while(max > 0) {
			pos++;
			max--;
			input[pos%input.length] = input[pos%input.length]+1;
		}

		return input;
	}

	private static String convert(int[] ints) {
		StringJoiner sj = new StringJoiner(",");
		Arrays.stream(ints).forEach(i -> sj.add(i+""));
		return sj.toString();
	}
}
