package be.johanbas.day08;

/**
 * Created by Johan Bas on 9/12/2015.
 */
public class Edge {

    int distence;
    Node from;
    Node to;

    public Edge(Node from, Node to, int distence) {
        this.distence = distence;
        this.from = from;
        this.to = to;
    }

    public int getDistence() {
        return distence;
    }

    public void setDistence(int distence) {
        this.distence = distence;
    }

    public Node getFrom() {
        return from;
    }

    public void setFrom(Node from) {
        this.from = from;
    }

    public Node getTo() {
        return to;
    }

    public void setTo(Node to) {
        this.to = to;
    }
}
