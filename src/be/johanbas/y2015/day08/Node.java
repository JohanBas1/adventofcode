package be.johanbas.day08;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Johan Bas on 9/12/2015.
 */
public class Node {

    String name;
    List<Edge> edges = new LinkedList<>();

    public Node(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addEdge(Edge e) {
        edges.add(e);
    }
}
