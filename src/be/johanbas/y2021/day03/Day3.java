package be.johanbas.y2021.day03;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Day3 {

    public static void main(String[] args) throws URISyntaxException, IOException {

        final List<String> strings = readInput();

        part1(strings);
        part2(strings);
    }

    private static void part2(List<String> strings) {
        List<String> oxygenInput = new LinkedList<>(strings);
        for (int i = 0; i < strings.get(0).length(); i++) {
            final int j = i;
            final String gamma = calculateGamma(oxygenInput);
            oxygenInput = oxygenInput.stream().filter(s -> s.charAt(j) == gamma.charAt(j)).collect(Collectors.toList());
            if(oxygenInput.size() == 1) {
                break;
            }
        }

        System.out.println("oxygenInput = " + oxygenInput.get(0));
        final int oxygenInputInt = Integer.parseInt(oxygenInput.get(0), 2);

        List<String> coInput = new LinkedList<>(strings);
        for (int i = 0; i < strings.get(0).length(); i++) {
            final int j = i;
            final String gamma = calculateGamma(coInput);
            coInput = coInput.stream().filter(s -> s.charAt(j) != gamma.charAt(j)).collect(Collectors.toList());
            if(coInput.size() == 1) {
                break;
            }
        }

        System.out.println("coInput = " + coInput);
        System.out.println("coInput = " + coInput.get(0));
        final int coInputInt = Integer.parseInt(coInput.get(0), 2);

        System.out.println("part2: " + coInputInt * oxygenInputInt);
    }

    private static void part1(List<String> strings) {
        String gamma = calculateGamma(strings);
        System.out.println("gamma = " + gamma);
        final int gammaInt = Integer.parseInt(gamma, 2);
        System.out.println("gamma = " + gammaInt);

        String epsilon = "";
        for (char c : gamma.toCharArray()) {
            epsilon += c == '1' ? "0" : "1";
        }

        System.out.println("epsilon = " + epsilon);
        final int epsilonInt = Integer.parseInt(epsilon, 2);
        System.out.println("epsilon = " + epsilonInt);

        System.out.println("part1:" + gammaInt * epsilonInt);
    }

    private static String calculateGamma(List<String> strings) {
        List<List<Character>> input = new LinkedList<>();
        for (int i = 0; i < strings.get(0).length(); i++) {
            input.add(new LinkedList<>());
        }

        for (String string : strings) {
            final char[] chars = string.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                input.get(i).add(chars[i]);
            }
        }

        String gamma = "";
        for (List<Character> characters : input) {
            int count = 0;
            for (Character character : characters) {
                if(character.equals('0')) {
                    count++;
                }
            }

            if(count > strings.size() / 2) {
                gamma += "0";
            } else {
                gamma += "1";
            }
        }
        return gamma;
    }


    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day3.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
