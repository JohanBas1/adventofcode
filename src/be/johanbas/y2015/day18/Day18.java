package be.johanbas.y2015.day18;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/*

--- Day 18: Like a GIF For Your Yard ---

After the million lights incident, the fire code has gotten stricter: now, at most ten thousand lights are allowed. You arrange them in a 100x100 grid.

Never one to let you down, Santa again mails you instructions on the ideal lighting configuration. With so few lights, he says, you'll have to resort to animation.

Start by setting your lights to the included initial configuration (your puzzle input). A # means "on", and a . means "off".

Then, animate your grid in steps, where each step decides the next configuration based on the current one.
Each light's next state (either on or off) depends on its current state and the current states of the eight lights adjacent to it (including diagonals).
Lights on the edge of the grid might have fewer than eight neighbors; the missing ones always count as "off".

For example, in a simplified 6x6 grid, the light marked A has the neighbors numbered 1 through 8, and the light marked B, which is on an edge, only has the neighbors marked 1 through 5:

1B5...
234...
......
..123.
..8A4.
..765.
The state a light should have next is based on its current state (on or off) plus the number of neighbors that are on:

A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
All of the lights update simultaneously; they all consider the same current state before moving to the next.

Here's a few steps from an example configuration of another 6x6 grid:

Initial state:
.#.#.#
...##.
#....#
..#...
#.#..#
####..

After 1 step:
..##..
..##.#
...##.
......
#.....
#.##..

After 2 steps:
..###.
......
..###.
......
.#....
.#....

After 3 steps:
...#..
......
...#..
..##..
......
......

After 4 steps:
......
......
..##..
..##..
......
......
After 4 steps, this example has four lights on.

In your grid of 100x100 lights, given your initial configuration, how many lights are on after 100 steps?




--- Part Two ---

You flip the instructions over; Santa goes on to point out that this is all just an implementation of Conway's Game of Life.
At least, it was, until you notice that something's wrong with the grid of lights you bought: four lights, one in each corner, are stuck on and can't be turned off.
The example above will actually run like this:

After 5 steps, this example now has 17 lights on.

In your grid of 100x100 lights, given your initial configuration, but with the four corners always in the on state, how many lights are on after 100 steps?

 */
public class Day18 {

    private final static int GRID_SIZE = 100;

    public static void main(String[] args) throws IOException, URISyntaxException {

        List<String> input = readInput();

        boolean[][] grid = new boolean[GRID_SIZE][GRID_SIZE];

        for (int row = 0; row < input.size(); row++) {
            char[] line = input.get(row).toCharArray();
            for (int column = 0; column < line.length; column++) {
                grid[row][column] = line[column] == '.' ? false : true;
            }
        }

        // step 2
        grid[0][0] = true;
        grid[0][GRID_SIZE-1] = true;
        grid[GRID_SIZE-1][0] = true;
        grid[GRID_SIZE-1][GRID_SIZE-1] = true;

        for(int i=0; i<100; i++)
            grid = animateGrid(grid);

        int countOn = 0;
        for (boolean[] row : grid) {
            for (boolean column : row) {
                if(column) {
                    countOn++;
                }
            }
        }

        System.out.println(countOn);
    }

    private static boolean[][] animateGrid(boolean[][] grid) {
        boolean[][] newGrid = new boolean[GRID_SIZE][GRID_SIZE];

        for (int row = 0; row < grid.length; row++) {
            for (int column = 0; column < grid[row].length; column++) {
                newGrid[row][column] = calculateNewValue(row,column,grid);
            }
        }

        // step 2
        newGrid[0][0] = true;
        newGrid[0][GRID_SIZE-1] = true;
        newGrid[GRID_SIZE-1][0] = true;
        newGrid[GRID_SIZE-1][GRID_SIZE-1] = true;

        return newGrid;
    }

//    A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
//    A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
    private static boolean calculateNewValue(int row, int column, boolean[][] grid) {

        int countOn = 0;

        // top left
        if(row > 0 && column > 0 && grid[row-1][column-1])
            countOn++;

        // top middle
        if(row > 0 && grid[row-1][column])
            countOn++;

        // top right
        if(row > 0 && column < GRID_SIZE-1 && grid[row-1][column+1])
            countOn++;

        // left middle
        if(column > 0 && grid[row][column-1])
            countOn++;

        // right middle
        if(column < GRID_SIZE-1 && grid[row][column+1])
            countOn++;

        // bottom left
        if(row < GRID_SIZE-1 && column > 0 && grid[row+1][column-1])
            countOn++;

        // bottom middle
        if(row < GRID_SIZE-1 && grid[row+1][column])
            countOn++;

        // bottom right
        if(row < GRID_SIZE-1 && column < GRID_SIZE-1 && grid[row+1][column+1])
            countOn++;

        if(grid[row][column])
            if(countOn == 2 || countOn == 3)
                return true;

        if(!grid[row][column])
            if(countOn == 3)
                return true;

        return false;
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day18.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
