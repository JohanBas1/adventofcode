package be.johanbas.y2015.day23;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day23 {

    public static void main(String[] args) throws URISyntaxException, IOException {

//        var cpu = new CpuState(0, 0, 0); // b=170
        var cpu = new CpuState(1, 0, 0); //
        var input = readInput().stream().map(Instruction::new).toList();

        while (cpu.offset() >= 0 && cpu.offset() < input.size()) {
            cpu = input.get(cpu.offset()).apply(cpu);
//            System.out.println("cpu = " + cpu);
        }

        System.out.println(cpu);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day23.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
