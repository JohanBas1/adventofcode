package be.johanbas.y2015.day24;

import java.math.BigInteger;
import java.util.List;

record PackagesGroup(List<BigInteger> packages) {

    public BigInteger quantumEntanglement() {
        return packages.stream().reduce(BigInteger.ONE, BigInteger::multiply);
    }
}
