package be.johanbas.y2017.day10;

/**
 * Created by Johan Bas on 12/12/2017.
 */
public class Day10 {

	public static void main(String[] args) {

		int size = 5;
		int[] list = new int[size];
		int[] input = {3, 4, 1, 5};
		int length = input.length;
		int skipSize = 0;

		for (int i = 0; i < size; i++) {
			list[i] = i;
		}

		print(list);
		swap(size, list, skipSize, 3);
		print(list);
		swap(size, list, skipSize, 4);
		print(list);

	}

	private static void swap(int size, int[] list, int skipSize, int pos) {
		for (int y = skipSize; y < (pos + skipSize) / 2; y++) {
			int first = y % size;
			int last = (pos + skipSize - y - 1) % size;

			int temp = list[first];
			list[first] = list[last];
			list[last] = temp;

			skipSize++;
		}
	}

	private static void print(int[] list) {
		for (int i : list) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
}
