package be.johanbas.y2020.day17;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Day17 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        final List<String> list = readInput();
        HyperCube start = new HyperCube();
        for (int x = 0; x < list.size(); x++) {
            final char[] chars = list.get(x).toCharArray();
            for (int y = 0; y < chars.length; y++) {
                if(chars[y] == '#') {
                    start.markePosActive(x,y,0, 0);
                }
            }
        }

        for(int i=0; i<6; i++) {
            start = start.cycle();
        }

        System.out.println("start.totalActiveCubes = " + start.totalActiveCubes);

    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day17.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
