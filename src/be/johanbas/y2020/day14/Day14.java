package be.johanbas.y2020.day14;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day14 {

    private static Map<String,Long> memoryBank = new HashMap<>();
    private static Pattern r = Pattern.compile("mem\\[(\\d+)] = (\\d+)");

    public static void main(String[] args) throws URISyntaxException, IOException {


//        part1();

        part2();
    }

    private static void part2() throws URISyntaxException, IOException {
        String mask = "";
        for (String line : readInput()) {
            if(line.startsWith("mask")) {
                mask = line.substring(7);
            } else {
                Matcher m = r.matcher(line);
                if (m.find()) {
                    var memoryAddresses = applyFloatingMemoryMask(mask, Integer.parseInt(m.group(1)));
                    memoryAddresses.forEach(l -> memoryBank.put(l+"", Long.parseLong(m.group(2))));
                } else {
                    System.out.println("no match");
                }
            }
        }

        var sum = memoryBank.values().stream().reduce(0L, (x, y) -> x + y);
        System.out.println("sum = " + sum);
    }

    private static List<Long> applyFloatingMemoryMask(String mask, int value) {
        String binary = String.format("%36s", Integer.toBinaryString(value)).replace(' ', '0');

        /*
    If the bitmask bit is 0, the corresponding memory address bit is unchanged.
    If the bitmask bit is 1, the corresponding memory address bit is overwritten with 1.
    If the bitmask bit is X, the corresponding memory address bit is floating.
         */

        String result = "";
        for(int i=0; i<36; i++) {
            if(mask.charAt(i) == 'X') {
                result += 'X';
            } else if(mask.charAt(i) == '0'){
                result += binary.charAt(i);
            } else if(mask.charAt(i) == '1') {
                result += 1;
            }
        }

        List<String> floatingResults = new LinkedList<>();
        floatingResults.add(result);

        while(floatingResults.stream()
                .anyMatch(s -> s.contains("X"))) {

            floatingResults = floatingResults.stream()
                    .map(Day14::changeFirstFloatingBit)
                    .flatMap(List::stream)
                    .toList();
        }

        return floatingResults.stream().map(s -> Long.parseLong(s,2)).toList();
    }

    private static List<String> changeFirstFloatingBit(String floatingString) {
        if(floatingString.contains("X")) {
            return List.of(
                floatingString.replaceFirst("X", "0"),
                floatingString.replaceFirst("X", "1"));
        } else {
            return List.of(floatingString);
        }
    }

    private static void part1() throws URISyntaxException, IOException {

        String mask = "";
        for (String line : readInput()) {
            if(line.startsWith("mask")) {
                mask = line.substring(7);
            } else {
                Matcher m = r.matcher(line);
                if (m.find()) {
                    long transformedValue = applyValueMask(mask, Integer.parseInt(m.group(2)));
                    memoryBank.put(m.group(1), transformedValue);
                } else {
                    System.out.println("no match");
                }
            }
        }

        var sum = memoryBank.values().stream().reduce(0L, (x, y) -> x + y);
        System.out.println("sum = " + sum);
    }

    private static long applyValueMask(String mask, int value) {
        String binary = String.format("%36s", Integer.toBinaryString(value)).replace(' ', '0');

        String result = "";
        for(int i=0; i<36; i++) {
            if(mask.charAt(i) == 'X') {
                result += binary.charAt(i);
            } else {
                result += mask.charAt(i);
            }
        }

        return Long.parseLong(result,2);
    }


    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day14.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
