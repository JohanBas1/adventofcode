package be.johanbas.y2015.day22;

public class PlayState implements Comparable<PlayState>, Cloneable {

    private int playerHitpoints;
    private int playerMana;
    private int bossHitpoints;
    private int bossDamage;

    private int shieldActive = 0;
    private int poisonActive = 0;
    private int rechargeActive = 0;

    // With the same starting stats for you and the boss, what is the least amount of mana you can spend and still win the fight?
    private Integer manaSpend = 0;

    public PlayState(int playerHitpoints, int playerMana, int bossHitpoints, int bossDamage) {
        this.playerHitpoints = playerHitpoints;
        this.playerMana = playerMana;
        this.bossHitpoints = bossHitpoints;
        this.bossDamage = bossDamage;
    }

    public void handleEffects() {
        if(rechargeActive > 0) {
            playerMana += 101;
            rechargeActive = rechargeActive -1;
            debug("Recharge provides 101 mana; its timer is now "+rechargeActive);
        }

        if(poisonActive > 0) {
            bossHitpoints = bossHitpoints - 3;
            poisonActive = poisonActive - 1;
            debug("Poison deals 3 damage; its timer is now "+poisonActive);
            if(bossHitpoints < 1) {
                System.out.println("boss is dood door poison, total mana:" + manaSpend);
                System.exit(0);
            }
        }

        if(shieldActive > 0) {
            shieldActive = shieldActive - 1;
            debug("Shield's timer is now "+shieldActive);
        }
    }

    public boolean bossTurn() {
        debug("");
        debug("-- Boss turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");

        handleEffects();
        
        if(shieldActive > 0) {
            playerHitpoints = playerHitpoints - Math.max(bossDamage - 7, 1);
            debug("Boss attacks for "+Math.max(bossDamage - 7, 1)+" damage.");

        } else {
            playerHitpoints = playerHitpoints - bossDamage;
            debug("Boss attacks for "+bossDamage+" damage.");
        }
        
        return playerHitpoints < 1;
    }

    // Magic Missile costs 53 mana. It instantly does 4 damage.
    public boolean playMagicMissile() {
        if(!canPlayMagicMissile()) {
            throw new IllegalStateException("can not play magic missiles, low on mana");
        }
        debug("");
        debug("-- Player turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");
//        handleEffects();
        debug("Player casts Magic Missile, dealing "+4+" damage.");

        manaSpend = manaSpend + 53;
        playerMana = playerMana - 53;
        bossHitpoints = bossHitpoints - 4;
        if(bossHitpoints < 1) System.out.println("boss dood in " + manaSpend);
        return bossHitpoints < 1;
    }

    public boolean canPlayMagicMissile() {
        return playerMana >= 53;
    }

    // Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
    public boolean playDrain() {
        if(!canPlayDrain()) {
            throw new IllegalStateException("can not play drain, low on mana");
        }
        debug("");
        debug("-- Player turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");
//        handleEffects();
        debug("Player casts Drain, dealing 2 damage, and healing 2 hit points.");

        manaSpend = manaSpend + 73;
        playerMana = playerMana - 73;
        bossHitpoints = bossHitpoints - 2;
        playerHitpoints = playerHitpoints + 2;
        if(bossHitpoints < 1) System.out.println("boss dood in " + manaSpend);
        return bossHitpoints < 1;
    }

    public boolean canPlayDrain() {
        return playerMana >= 73;
    }

    // Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
    // You cannot cast a spell that would start an effect which is already active.
    public boolean playShield() {
        if(!canPlayShield()) {
            throw new IllegalStateException("can not play shield, low on mana");
        }

        debug("");
        debug("-- Player turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");
//        handleEffects();
        debug("Player casts Shield, increasing armor by 7.");

        if(shieldActive > 0) {
            throw new IllegalStateException("Shield is al actief!");
        }

        manaSpend = manaSpend + 113;
        playerMana = playerMana - 113;
        shieldActive = 6;
        if(bossHitpoints < 1) System.out.println("boss dood in " + manaSpend);
        return bossHitpoints < 1;
    }

    public boolean canPlayShield() {
        return playerMana >= 113;
    }

    // Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
    public boolean playPoison() {
        if(!canPlayPoison()) {
            throw new IllegalStateException("can not play poison, low on mana");
        }

        debug("");
        debug("-- Player turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");
//        handleEffects();
        debug("Player casts Poison.");


        if(poisonActive > 0) {
            throw new IllegalStateException("Poison is al actief!");
        }

        manaSpend = manaSpend + 173;
        playerMana = playerMana - 173;
        poisonActive = 6;
        if(bossHitpoints < 1) System.out.println("boss dood in " + manaSpend);
        return bossHitpoints < 1;
    }

    public boolean canPlayPoison() {
        return playerMana >= 173;
    }

    // Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
    public void playRecharge() {
        if(!canplayRecharge()) {
            throw new IllegalStateException("can not play recharge, low on mana");
        }

        debug("");
        debug("-- Player turn --");
        debug("- Player has "+playerHitpoints+" hit points, 0 armor, "+playerMana+" mana");
        debug("- Boss has "+bossHitpoints+" hit points");
//        handleEffects();
        debug("Player casts Recharge.");

        if(rechargeActive > 0) {
            throw new IllegalStateException("Recharge is al actief!");
        }
        manaSpend = manaSpend + 229;
        playerMana = playerMana - 229;
        rechargeActive = 5;
    }

    public boolean canplayRecharge() {
        return playerMana >= 229;
    }

    @Override
    public int compareTo(PlayState o) {
        return this.manaSpend.compareTo(o.manaSpend);
    }

    public PlayState clone() throws CloneNotSupportedException {
        return (PlayState) super.clone();
    }

    public int getTotalManaSpend() {
        return manaSpend;
    }
    
    public boolean isShieldActive() {
        return shieldActive > 0;
    }

    public boolean isRechargeActive() {
        return rechargeActive > 0;
    }

    public boolean isPoisonActive() {
        return poisonActive > 0;
    }

    public void debug(String log) {
//        System.out.println(log);
    }

    public boolean surviveHardmode() {
        playerHitpoints = playerHitpoints - 1;
        return playerHitpoints > 0;
    }
}
