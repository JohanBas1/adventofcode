package be.johanbas.y2020.day17;

import java.util.HashMap;
import java.util.Map;

public class HyperCube {

    Map<Integer, Map<Integer, Map<Integer, Map<Integer, Boolean>>>> cube = new HashMap<>();

    int xMin = Integer.MAX_VALUE;
    int xMax = Integer.MIN_VALUE;

    int yMin = Integer.MAX_VALUE;
    int yMax = Integer.MIN_VALUE;

    int zMin = Integer.MAX_VALUE;
    int zMax = Integer.MIN_VALUE;

    int wMin = Integer.MAX_VALUE;
    int wMax = Integer.MIN_VALUE;

    int totalActiveCubes = 0;

    public void markePosActive(int x, int y, int z, int w) {
        Map<Integer, Map<Integer, Map<Integer, Boolean>>> xMap = cube.computeIfAbsent(x, a -> new HashMap<>());
        Map<Integer, Map<Integer, Boolean>> yMap = xMap.computeIfAbsent(y, a -> new HashMap<>());
        Map<Integer, Boolean> zMap = yMap.computeIfAbsent(z, a -> new HashMap<>());
        zMap.put(w, true);

        if (x < xMin) {
            xMin = x;
        }
        if (x > xMax) {
            xMax = x;
        }

        if (y < yMin) {
            yMin = y;
        }
        if (y > yMax) {
            yMax = y;
        }

        if (z < zMin) {
            zMin = z;
        }
        if (z > zMax) {
            zMax = z;
        }

        if (w < wMin) {
            wMin = w;
        }
        if (w > wMax) {
            wMax = w;
        }


        totalActiveCubes++;
    }

    public boolean getPosState(int x, int y, int z, int w) {
        Map<Integer, Map<Integer, Map<Integer, Boolean>>> xMap = cube.getOrDefault(x, new HashMap<>());
        Map<Integer, Map<Integer, Boolean>> yMap = xMap.getOrDefault(y, new HashMap<>());
        Map<Integer, Boolean> zMap = yMap.getOrDefault(z, new HashMap<>());
        return zMap.getOrDefault(w, false);
    }

    public int countNeighbours(int x, int y, int z, int w) {
        int active = 0;

        for (int dx = -1; dx <= 1; dx++) {
            for (int dy = -1; dy <= 1; dy++) {
                for (int dz = -1; dz <= 1; dz++) {
                    for (int dw = -1; dw <= 1; dw++) {

                        if (dx != 0 || dy != 0 || dz != 0 || dw != 0) {
                            if (getPosState(x + dx, y + dy, z + dz, w + dw)) {
                                active++;
                            }
                        }

                    }
                }
            }
        }
        return active;
    }

    public HyperCube cycle() {

        HyperCube c = new HyperCube();
        for (int x = xMin - 1; x <= xMax + 1; x++) {
            for (int y = yMin - 1; y <= yMax + 1; y++) {
                for (int z = zMin - 1; z <= zMax + 1; z++) {
                    for (int w = wMin - 1; w <= wMax + 1; w++) {

                        int neighbours = countNeighbours(x, y, z, w);
                        boolean state = getPosState(x, y, z, w);

                        if (state) {
                            if (neighbours == 2 || neighbours == 3) {
                                c.markePosActive(x, y, z, w);
                            }
                        } else {
                            if (neighbours == 3) {
                                c.markePosActive(x, y, z, w);
                            }
                        }

                    }
                }
            }
        }

        return c;
    }

}
