package be.johanbas.y2018.day04;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day04 {

    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> strings = readInput();

        Set<Log> logs = new TreeSet<>();
        for (String string : strings) {
            logs.add(new Log(string));
        }

        for (Log log : logs) {
            System.out.println(log.stamp + " - " + log.log);
        }

        List<Guard> guards = new LinkedList<>();
        Guard g = null;
        for (Log log : logs) {
            if(log.log.startsWith("Guard")) {
                g = new Guard(log.log.split(" ")[1]);
                guards.add(g);
            } else {
                g.logs.add(log);
            }
        }

        Guard guard = guards.stream()
                .max(Comparator.comparing(Guard::getMinutesAsleep))
                .get();

        System.out.println("guard = " + guard.toString());
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day04.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
