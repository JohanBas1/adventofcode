package be.johanbas.y2021.day04;

import java.util.List;

public class BingoBoard {

    private Integer[][] matrix = new Integer[5][5];

    public BingoBoard(List<String> input)  {
        input.remove(0);

        for (int i = 0; i < input.size(); i++) {
            final String row = input.get(i);

            final String een = row.substring(0, 2).replace(" ", "");
            matrix[i][0] = Integer.parseInt(een);
            final String twee = row.substring(3, 5).replace(" ", "");
            matrix[i][1] = Integer.parseInt(twee);
            final String drie = row.substring(6, 8).replace(" ", "");
            matrix[i][2] = Integer.parseInt(drie);
            final String vier = row.substring(9, 11).replace(" ", "");
            matrix[i][3] = Integer.parseInt(vier);
            final String vijf = row.substring(12, 14).replace(" ", "");
            matrix[i][4] = Integer.parseInt(vijf);
        }
    }

    public void makeMove(int move) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if(matrix[i][j] == (move)) {
                    matrix[i][j] = -1;
                }
            }
        }
    }

    public boolean hasBingo() {
        for (int i = 0; i < 5; i++) {
            var rowSum = 0;
            var colSum = 0;

            for (int j = 0; j < 5; j++) {
                rowSum += matrix[i][j];
                colSum += matrix[j][i];
            }

            if(rowSum == -5 || colSum == -5) {
                return true;
            }
        }

        return false;
    }

    public int calculateBoardScore() {

        int sum = 0;

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if(matrix[i][j] > 0) {
                    sum += matrix[i][j];
                }
            }
        }

        return sum;
    }
}
