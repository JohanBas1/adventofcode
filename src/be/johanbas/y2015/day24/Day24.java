package be.johanbas.y2015.day24;

import be.johanbas.y2015.day23.Day23;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Day24 {

    public static void main(String[] args) throws URISyntaxException, IOException {
//        Integer[] input = {1,2,3,4,5,7,8,9,10,11};
        var input = readInput().stream().map(Integer::parseInt).toArray(Integer[]::new);

        // look at groups that can make up this sum with the minimum of numbers
        int sum = Arrays.stream(input).reduce(0, Integer::sum) / 4;
        System.out.println("🎯 Sum: " + sum);
        System.out.println("🔍 mogelijke combinaties van de te zoeken som:");

        List<PackagesGroup> configs = new LinkedList<>();
        for (int i = 0; i < input.length; i++) {
            for (int j = i + 1; j < input.length; j++) {
// example
//                if(input[i] + input[j] == sum) {
//                    System.out.println("😎 Combination found: " + input[i] + " " + input[j]);
//                }

                for (int k = j + 1; k < input.length; k++) {
                    for (int l = k + 1; l < input.length; l++) {
// part 1
//                        for (int m = l+1; m < input.length; m++) {
//                            for (int n = m+1; n < input.length; n++) {
//                                if(input[i] + input[j] + input[k] + input[l] + input[m] + input[n] == sum) {
//                                    System.out.println("😎 Combination found: " + input[i] + " " + input[j] + " " + input[k] + " " + input[l] + " " + input[m] + " " + input[n]);
//                                    configs.add(new PackagesGroup(
//                                            List.of(BigInteger.valueOf(input[i]),
//                                                    BigInteger.valueOf(input[j]),
//                                                    BigInteger.valueOf(input[k]),
//                                                    BigInteger.valueOf(input[l]),
//                                                    BigInteger.valueOf(input[m]),
//                                                    BigInteger.valueOf(input[n])
//                                            )));
//                                }
//                            }
//                        }

                        // part 2
                        if (input[i] + input[j] + input[k] + input[l] == sum) {
                            System.out.println("😎 Combination of 4 found: " + input[i] + " " + input[j] + " " + input[k] + " " + input[l]);
                            configs.add(new PackagesGroup(
                                    List.of(BigInteger.valueOf(input[i]),
                                            BigInteger.valueOf(input[j]),
                                            BigInteger.valueOf(input[k]),
                                            BigInteger.valueOf(input[l])
                                            )));
                        }
                    }
                }
            }
        }

        PackagesGroup min = configs.stream().min(Comparator.comparing(PackagesGroup::quantumEntanglement)).get();
        System.out.println("minimum QuantumEntanglement = " + min.quantumEntanglement());

    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day24.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
