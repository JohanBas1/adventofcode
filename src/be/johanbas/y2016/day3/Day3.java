package be.johanbas.day3;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Johan Bas on 5/12/2016.
 */
public class Day3 {

	static int count = 0;
	static int count2 = 0;

	public static void main(String[] args) throws IOException, URISyntaxException {

		List<String> input = readInput();
		input.forEach(Day3::checkTriangle);

		System.out.println("PART 1:");
		System.out.println(count);

		for (int i = 0; i < input.size(); i=i+3) {
			checkTrianglePart2(input.get(i), input.get(i+1), input.get(i+2));
		}

		System.out.println("PART 2:");
		System.out.println(count2);

	}

	private static void checkTrianglePart2(String s, String s1, String s2) {

		String[] temp1 = s.replaceAll("\\s+", " ").split(" ");

		int x1 = Integer.parseInt(temp1[1]);
		int x2 = Integer.parseInt(temp1[2]);
		int x3 = Integer.parseInt(temp1[3]);

		String[] temp2 = s1.replaceAll("\\s+", " ").split(" ");

		int y1 = Integer.parseInt(temp2[1]);
		int y2 = Integer.parseInt(temp2[2]);
		int y3 = Integer.parseInt(temp2[3]);

		String[] temp3 = s2.replaceAll("\\s+", " ").split(" ");

		int z1 = Integer.parseInt(temp3[1]);
		int z2 = Integer.parseInt(temp3[2]);
		int z3 = Integer.parseInt(temp3[3]);

		if(checkTriangle(x1,y1,z1)) {
			count2++;
		}
		if(checkTriangle(x2,y2,z2)) {
			count2++;
		}
		if(checkTriangle(x3,y3,z3)) {
			count2++;
		}
	}

	private static void checkTriangle(String input) {
		String[] temp = input.replaceAll("\\s+", " ").split(" ");

		int x = Integer.parseInt(temp[1]);
		int y = Integer.parseInt(temp[2]);
		int z = Integer.parseInt(temp[3]);

		if(checkTriangle(x, y, z)) {
			count++;
		}
	}

	private static boolean checkTriangle(int x, int y, int z) {
		if(x+y > z && y+z > x && x+z > y) {
			return true;
		}
		return false;
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day3.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}
