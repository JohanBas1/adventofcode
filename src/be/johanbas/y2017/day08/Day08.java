package be.johanbas.y2017.day08;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * Created by Johan Bas on 8/12/2017.
 */
public class Day08 {

	static Map<String,Integer> regs = new HashMap<>();

	public static void main(String[] args) throws IOException, URISyntaxException {

		List<String> input = readInput();
		for (String s : input) {
			regs.put(s.split(" ")[0],0); // init regs
		}

		int max2 = 0;
		for (String s : input) {
			String[] split = s.split(" ");

			String reg = split[0];
			boolean opAdd = split[1].equals("inc");
			int val = Integer.parseInt(split[2]);

			String opReg = split[4];
			String opOp = split[5];
			int opVal = Integer.parseInt(split[6]);

			switch (opOp) {
				case ">":
					updateRegistry((x,y) -> x > y, reg, opAdd, val, opReg, opVal);
					break;
				case "<":
					updateRegistry((x,y) -> x < y, reg, opAdd, val, opReg, opVal);
					break;

				case ">=":
					updateRegistry((x,y) -> x >= y, reg, opAdd, val, opReg, opVal);
					break;

				case "==":
					updateRegistry(Integer::equals, reg, opAdd, val, opReg, opVal);
					break;

				case "<=":
					updateRegistry((x,y) -> x <= y, reg, opAdd, val, opReg, opVal);
					break;

				case "!=":
					updateRegistry((x,y) -> !x.equals(y),reg, opAdd, val, opReg, opVal);
					break;

				default:
					System.out.println("unsupported op " + opOp);
			}

			for (Map.Entry<String, Integer> stringIntegerEntry : regs.entrySet()) {
				if(stringIntegerEntry.getValue() > max2) {
					max2 = stringIntegerEntry.getValue();
				}
			}
		}

		int max = 0;
		for (Map.Entry<String, Integer> stringIntegerEntry : regs.entrySet()) {
			if(stringIntegerEntry.getValue() > max) {
				max = stringIntegerEntry.getValue();
			}
		}

		System.out.println(max);
		System.out.println(max2);
	}

	private static void updateRegistry(BiFunction<Integer,Integer,Boolean> f, String reg, boolean opAdd, int val, String opReg, int opVal) {
		if(f.apply(regs.get(opReg), opVal)) {
			if(opAdd) {
				regs.put(reg, regs.get(reg)+val);
			} else {
				regs.put(reg, regs.get(reg)-val);
			}
		}
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day08.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}
