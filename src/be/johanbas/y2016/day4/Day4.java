package be.johanbas.day4;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Johan Bas on 5/12/2016.
 */
public class Day4 {

	public static void main(String[] args) throws IOException, URISyntaxException {

		int sum = 0;
		List<String> input = readInput();
		for (String s : input) {
			sum += partOne(s);
		}

		System.out.println("PART 1");
		System.out.println(sum);

		System.out.println("PART 2");
		for (String s : input) {
			Room r = new Room(s);
			r.decrypt();
		}

	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day4.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}

	private static int partOne(String room) {
		Room r = new Room(room);

		if(r.isCorrectRoom()){
			return r.getId();
		} else {
			return 0;
		}
	}
}
