package be.johanbas.y2017.day05;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntFunction;

/**
 * Created by Johan Bas on 5/12/2017.
 */
public class Day05 {

	public static void main(String[] args) throws IOException, URISyntaxException {

//		Integer[] ints = {0,3,0,1,-3};

		List<String> input = readInput();
		int[] ints = input.stream()
				.mapToInt(Integer::parseInt)
				.toArray();

		process(ints.clone(), i -> i + 1); // 375042
		process(ints.clone(), i -> i >= 3 ? i - 1 : i + 1); // 28707598
	}

	private static void process(int[] ints, final Function<Integer, Integer> f) {
		int stepper = 0;
		int eval = 0;
		while(true) {

//			for (Integer anInt : ints) {
//				System.out.print(anInt + " ");
//			}
//			System.out.println("   eval " + eval);

			stepper++;
			int nextEval = ints[eval] + eval;

			ints[eval] = f.apply(ints[eval]);
			eval = nextEval;

			if(eval < 0 || eval > ints.length-1) {
				break;
			}
		}

		System.out.println(stepper);
	}

	private static List<String> readInput() throws URISyntaxException, IOException {
		URI input = Day05.class.getResource("input").toURI();
		return Files.readAllLines(Paths.get(input));
	}
}