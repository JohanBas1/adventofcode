package be.johanbas.y2015.day06;

import be.johanbas.y2015.day03.Point;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**

--- Day 6: Probably a Fire Hazard ---

Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0.
The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs.
Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square.
The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

For example:

turn on 0,0 through 999,999 would turn on (or leave on) every light.
toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.
After following the instructions, how many lights are lit?


--- Part Two ---

You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

The phrase turn on actually means that you should increase the brightness of those lights by 1.

The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

The phrase toggle actually means that you should increase the brightness of those lights by 2.

What is the total brightness of all lights combined after following Santa's instructions?

For example:

turn on 0,0 through 0,0 would increase the total brightness by 1.
toggle 0,0 through 999,999 would increase the total brightness by 2000000.
Although it hasn't changed, you can still get your puzzle input.


 */
public class Day6 {

    private static boolean[][] grid = new boolean[1000][1000];
    private static int[][] grid2 = new int[1000][1000];

    public static void main(String[] args) throws IOException, URISyntaxException {

        List<String> input = readInput();
//        input = new LinkedList<>();
//        input.add("turn on 0,0 through 999,999");
//        input.add("toggle 0,0 through 999,0");
//        input.add("turn off 499,499 through 500,500");

        for (String s : input) {
            if(s.startsWith("turn off")) {
                String[] split = s.split(" ");
                Point start = new Point(Integer.parseInt(split[2].split(",")[0]), Integer.parseInt(split[2].split(",")[1]));
                Point end = new Point(Integer.parseInt(split[4].split(",")[0]), Integer.parseInt(split[4].split(",")[1]));
                turnOff(start, end);
                turnOff2(start, end);
            } else if(s.startsWith("toggle")) {
                String[] split = s.split(" ");
                Point start = new Point(Integer.parseInt(split[1].split(",")[0]), Integer.parseInt(split[1].split(",")[1]));
                Point end = new Point(Integer.parseInt(split[3].split(",")[0]), Integer.parseInt(split[3].split(",")[1]));
                toggle(start, end);
                toggle2(start, end);
            } else if(s.startsWith("turn on")) {
                String[] split = s.split(" ");
                Point start = new Point(Integer.parseInt(split[2].split(",")[0]), Integer.parseInt(split[2].split(",")[1]));
                Point end = new Point(Integer.parseInt(split[4].split(",")[0]), Integer.parseInt(split[4].split(",")[1]));
                turnOn(start, end);
                turnOn2(start, end);
            }
        }

        System.out.println(getCount());
        System.out.println(getCount2());
    }

    private static int getCount() {
        int count = 0;
        for(int row=0; row<1000; row++) {
            for(int col=0; col<1000; col++) {
                if(grid[row][col]) count++;
            }
        }
        return count;
    }

    private static int getCount2() {
        int count = 0;
        for(int row=0; row<1000; row++) {
            for(int col=0; col<1000; col++) {
                count += grid2[row][col];
            }
        }
        return count;
    }

    private static void turnOff(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                grid[row][col] = false;
            }
        }
    }

    private static void toggle(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                grid[row][col] = !grid[row][col];
            }
        }
    }


    private static void turnOn(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                grid[row][col] = true;
            }
        }
    }


//    The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.
    private static void turnOff2(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                int inc = grid2[row][col] - 1;
                grid2[row][col] = inc >= 0 ? inc : 0;
            }
        }
    }

//    The phrase toggle actually means that you should increase the brightness of those lights by 2.
    private static void toggle2(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                grid2[row][col] = grid2[row][col] + 2;
            }
        }
    }

    //    The phrase turn on actually means that you should increase the brightness of those lights by 1.
    private static void turnOn2(Point start, Point end) {
        for (int row = start.getY(); row <= end.getY(); row++) {
            for (int col = start.getX(); col <= end.getX(); col++) {
                grid2[row][col] = grid2[row][col]+1;
            }
        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day6.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
