package be.johanbas.y2022.day05;

import be.johanbas.tools.Filez;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

public class Day05 {

//    private final static String stacksInput = "stacksExample.txt";
//    private final static String instructionsInput = "instructionsExample.txt";
//    private final static String containerPositions = " 1   2   3";

    private final static String stacksInput = "stacksInput.txt";
    private final static String instructionsInput = "instructionsInput.txt";
    private final static String containerPositions = " 1   2   3   4   5   6   7   8   9";

    private final static List<LinkedList<Character>> queues = new LinkedList<>();

    public static void main(String[] args) {
        var numberOfStacks = Integer.parseInt(containerPositions.substring(containerPositions.length()-1, containerPositions.length()));

        for(var i=0; i<numberOfStacks; i++) {
            queues.add(new LinkedList<>());
        }

        var stacks = Filez.readFile(stacksInput, Day05.class);
        stacks.forEach(Day05::parseStack);

        var moves = Filez.readFile(instructionsInput, Day05.class)
                .stream()
                .map(Day05::match)
                .toList();

        System.out.println("Parsing Done");

//        moves.forEach(Day05::applyMovePart1);
        moves.forEach(Day05::applyMovePart2);

        System.out.println("moves done");

        var characters = queues.stream().map(LinkedList::getFirst).toList();
        System.out.println("characters = " + characters);
    }

    private static void applyMovePart1(Move move) {
        var fromQueue = queues.get(move.from() - 1);
        var toQueue = queues.get(move.to() - 1);

        for(var i=0; i<move.stacks(); i++) {
            toQueue.addFirst(fromQueue.removeFirst());
        }
    }

    private static void applyMovePart2(Move move) {

        System.out.println(queues);

        var fromQueue = queues.get(move.from() - 1);
        var toQueue = queues.get(move.to() - 1);

        var temp = new LinkedList<Character>();

        for(var i=0; i<move.stacks(); i++) {
            temp.add(fromQueue.removeFirst());
        }

        Collections.reverse(temp);
        temp.forEach(toQueue::addFirst);
    }

    private static void parseStack(String input) {
        var chars = input.toCharArray();
        for (var i = 0; i < chars.length; i++) {
            if(chars[i] > 64 && chars[i] < 91) {
                var queueNumber = Integer.parseInt(containerPositions.toCharArray()[i]+"");
                queues.get(queueNumber-1).add(chars[i]);
            }
        }
    }

    public static Pattern movePattern = Pattern.compile("move (\\d+) from (\\d+) to (\\d+)");
    public static Move match(String x) {
        var matcher = movePattern.matcher(x);
        if(!matcher.matches()) {
            throw new RuntimeException("euhm ...");
        }

        var count = Integer.parseInt(matcher.group(1));
        var from = Integer.parseInt(matcher.group(2));
        var to = Integer.parseInt(matcher.group(3));
        return new Move(count, from, to);
    }
}

record Move(int stacks, int from, int to) {}