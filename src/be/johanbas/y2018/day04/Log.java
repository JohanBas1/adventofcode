package be.johanbas.y2018.day04;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Log implements Comparable {

    LocalDateTime stamp;
    String log;

    public Log(String input) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        stamp = LocalDateTime.parse(input.split("] ")[0].substring(1), formatter);
        log = input.split("] ")[1];
    }

    @Override
    public int compareTo(Object o) {
        return stamp.compareTo(((Log) o).stamp);
    }
}
