package be.johanbas.day2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by Johan Bas on 5/12/2016.
 */
public class Day2 {

	private static List<String> input = new LinkedList<>();

	public static void main(String[] args) {

		input.add("LUULRUULULLUDUDULDLUDDDLRURUDLRRDRDULRDDULLLRULLLURDDLRDLUUDDRURDDRDDDDRDULULLLLURDDLLRLUUDDDRLRRRDURLDDLRRLDUDRRRDLDLRRDLDLUURRLRULLULRUDRDLRUURLDRDLRLDULLLUDRDDRLURLUUDRLLLDRUUULLUULRUDDUDRDUURRRUDRLDDUURDUURUDRDDLULDDUDUDRRDDULUDULRDRULRLRLURURDULRUULLRDDDDRRUUDDDUUDRLLRUDRLRDLRRLULRLULRUDDULRLLLURLDDRLDDLRRLDRDDDRRLRUDRULUUDUURLDLRRULUDRDULDLLRRURRDDLRRRLULUDUUDDUDDLRDLRDRLRLDUDUDDUDLURRUURDRLRURLURRRLRLRRUDDUDDLUDRLUURUUDUUDDULRRLUUUDRLRLLUR");
		input.add("LDLLRRLDULDDRDDLULRRRDDUDUDRRLLRUUULRUDLLRRDDRRLDDURUUDLUDRRLDURDDRUDLUDUUDLDLLLDLLLDRLLDLRUULULLUUDULDUUULDDLRUDLLUDLUUULDRLUDRULUUDLDURDLDUULLRDUDRDLURULDLUUUDURLDDRLLDRLRDDDUDRUULLDLUDRRDDLDLUURUDDLDRURRLULUDDURLDRDRDUDDRRULRLDURULULRURDUURRUDRDDRDRLDRDUUDLRULRDDDULRURUDRUUULUUDDLRRDDDUDRLRUDRDLRRUDLUDRULDDUDLRLDDLDRLRDLULRDRULRLLRLUDUURULLLDDUULUUDDDUDRRULDDDULRUDRRLRLLLUDLULDUUULDDULDUUDLUULRDLDUDRUDLLDLDLLULDDDDLUDDUDRUDLRRRDDDDDLLRRDRUUDDDRRULRUDUUDRULLDLLLDDRDDUURLUUURUDRUDURLRUUUULUUURDRRRULDUULDLDDDRDDDDLLDRUDRDURLDDURDURULDDRLLRRLDUDRDURRLDRDLLULUUUD");
		input.add("LDDLRLRDDRLRUDDRDDUDRULUUULULDULRUULLRRDUULRDUUDDDRRULDDUDRLLLDULURDLDDRLLRURULULDLDULRDLDLRULUDLLDRUDLDURRDULDDRLRURDLLUDRDDDUDLUDULURULRDRLRULDLLRLDRRUDRDRUDRLDLRLUUURURRRLDDULLULLLRLRLULDLLRLDDRLDULURULRUURRUUURRUDRLRRURURDDDRULDULDLDLRRRLLDDRRURRULULULDRDULDRRULDUDRRLDULDRDURRDULLRRRLLLLRRLLRRRDRURDUULLURURURDDRRDRLLLULRRRDRLDRLDRDLLRUUDURRDRRDLLUDLDRLRLDLUDRDULRULRRLLRDLULDRLUDUUULLDRULDDLLRDUUUDRUUUUULUURDDLLDUURURRURLLURRDDUDUDRUUDDRDDRRLRLULRLRRRDRLLRRLLLDUULLUUDDLULLLDURRLLDRLDRDRLRRLRRULRRRRLRRRRRURUDULUULRDLLDRLRRDUURDRRUDRURRRDDRLDDLRLUDRDRDRRLDDDRDDRRRDUDULRURRDRDLLDRUD");
		input.add("UUUDLDDLRDLLLLRUUURDDLLURRUUURLUULLURUUDUDLDULULLRRRRLLLRDLLUDRUURDRURUDRURRLRLDRURLUDRLULRRURDDDURLLDULDLRRRDUUDDDRDLRUURRDRDRLRDLULRLDDRULRULDRDUDRUURLDLUDDULLLRURRLURLULDRRLUUURURLDLDDULLLRUUURDDDUURULULLUUUDUDRLLRRULUULDDDLLUDLURLLLRRULLURDRLUUDDLLDLLLUDULLRDRRRURDRUDUDUULUDURDLRUDLLRDDRURUDURLRULURDDURULLRDDRLRRDRLLULRDDDULRDLRULDDLRRDULDLUURRURUULRRDUURUDRRRRRLDULDLRURRULULDLRDDDRLLDURRULDUDUDRRRLUULRLUDURRRLRLDURRRRUULDRLUDDDUDURLURUDLLUDRDDDRLLURLRLDDURUUDDDUDUR");
		input.add("RURRRRURUDDRLURUDULRDUDDDUURULDRRRRURDLDRRLLDLUDLRRLRRUULLURULLRDLLRDDDDULLRLLDDLLRUDDULDUDLDURLRUULDDURURDURDLDRRULRURRRRRLRRLLUDURRURULRLRDLRLRRRLLURURDLLLDLDDULDLUDDLLLRUDDRDRLRUDRRLDDLRDLRLRLRLRRDUUURRUDRRLDLRRUULULLUDRRRUDLURDRUULDRDRRLUULULDDLURRLDULLURLDRLDULDRLLDLUUULLULRRDDRURRURLDLDRRLLLLLUDUURUULURLRDDDLRRRRLLLURUDLDDRDDRRUDURUULDRRULLLRRLRULLLRLDDLLRRLRURLRDRUDULLDDLDDDDDLDURURDLULRDDLRDLLRURLLRDLRUDDRDRRDURDURLUDRLDUDDDRRURRLUULURULLRLRDLRRLRURULLDDURLLRRRUDDRDLULURRRUUUULUULRRLLDLRUUURLLURLUURRLRL");

		System.out.println("PART 1");
		int i = parseLine(input.get(0), 5);
		System.out.println(i);
		i = parseLine(input.get(1), i);
		System.out.println(i);
		i = parseLine(input.get(2), i);
		System.out.println(i);
		i = parseLine(input.get(3), i);
		System.out.println(i);
		i = parseLine(input.get(4), i);
		System.out.println(i);

		System.out.println("PART 2");
		String j = parseLinePart2(input.get(0), "5");
		System.out.println(j);
		j = parseLinePart2(input.get(1), j);
		System.out.println(j);
		j = parseLinePart2(input.get(2), j);
		System.out.println(j);
		j = parseLinePart2(input.get(3), j);
		System.out.println(j);
		j = parseLinePart2(input.get(4), j);
		System.out.println(j);
	}

	private static int parseLine(String input, int position) {

		// 1 2 3
		// 4 5 6
		// 7 8 9

		for (int i = 0; i < input.length(); i++){
			char code = input.charAt(i);

			if(code == 'L') {
				if(position == 2 || position == 3 || position == 5 || position == 6 || position == 8 || position == 9) {
					position -= 1;
				}

			} else if(code == 'U') {
				if(position == 4 || position == 5 || position == 6 || position == 7 || position == 8 || position == 9) {
					position -= 3;
				}

			} else if(code == 'D') {
				if(position == 1 || position == 2 || position == 3 || position == 4 || position == 5 || position == 6) {
					position += 3;
				}

			} else if(code == 'R') {
				if(position == 1 || position == 2 || position == 4 || position == 5 || position == 7 || position == 8) {
					position += 1;
				}
			}
		}

		return position;
	}

	private static String parseLinePart2(String input, String position) {

//		    1
//		  2 3 4
//		5 6 7 8 9
//		  A B C
//		    D

		Map<String, String> goLeft = new HashMap<>();
		goLeft.put("1","1");
		goLeft.put("2","2");
		goLeft.put("3","2");
		goLeft.put("4","3");
		goLeft.put("5","5");
		goLeft.put("6","5");
		goLeft.put("7","6");
		goLeft.put("8","7");
		goLeft.put("9","8");
		goLeft.put("A","A");
		goLeft.put("B","A");
		goLeft.put("C","B");
		goLeft.put("D","D");

		Map<String, String> goDown = new HashMap<>();
		goDown.put("1","3");
		goDown.put("2","6");
		goDown.put("3","7");
		goDown.put("4","8");
		goDown.put("5","5");
		goDown.put("6","A");
		goDown.put("7","B");
		goDown.put("8","C");
		goDown.put("9","9");
		goDown.put("A","A");
		goDown.put("B","D");
		goDown.put("C","C");
		goDown.put("D","D");

		Map<String, String> goRight = new HashMap<>();
		goRight.put("1","1");
		goRight.put("2","3");
		goRight.put("3","4");
		goRight.put("4","4");
		goRight.put("5","6");
		goRight.put("6","7");
		goRight.put("7","8");
		goRight.put("8","9");
		goRight.put("9","9");
		goRight.put("A","B");
		goRight.put("B","C");
		goRight.put("C","C");
		goRight.put("D","D");

		Map<String, String> goUp = new HashMap<>();
		goUp.put("1","1");
		goUp.put("2","2");
		goUp.put("3","1");
		goUp.put("4","4");
		goUp.put("5","5");
		goUp.put("6","2");
		goUp.put("7","3");
		goUp.put("8","4");
		goUp.put("9","9");
		goUp.put("A","6");
		goUp.put("B","7");
		goUp.put("C","8");
		goUp.put("D","B");


		for (int i = 0; i < input.length(); i++){
			String code = input.charAt(i)+"";

			if(code.equals("L")) {
				position = goLeft.get(position);

			} else if(code.equals("U")) {
				position = goUp.get(position);

			} else if(code.equals("D")) {
				position = goDown.get(position);

			} else if(code.equals("R")) {
				position = goRight.get(position);

			}
		}

		return position;
	}
}
