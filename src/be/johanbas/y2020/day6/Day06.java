package be.johanbas.y2020.day6;

import com.google.common.primitives.Chars;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Day06 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        var inputs = readInput();
        part1(inputs);

        part2(inputs);
    }

    private static void part2(List<String> inputs) {
        List<List<String>> groups = new LinkedList<>();
        List<String> group = new LinkedList<>();
        for (String input : inputs) {
            if(input.trim().equals("")) {
                groups.add(group);
                group = new LinkedList<>();
            } else {
                group.add(input);
            }
        }
        groups.add(group);
//        System.out.println("groups = " + groups);

        int sum2 = 0;
        for (List<String> groep : groups) {
            for (char c : groep.get(0).toCharArray()) {
                boolean charPresent = true;
                for (String declaration : groep) {
                    if(!(declaration.contains(c+""))) {
                        charPresent = false;
                    }
                }
                if(charPresent) {
                    sum2++;
                }
            }
        }

        System.out.println("sum2 = " + sum2);
    }

    private static void part1(List<String> inputs) {
        var joiner = new StringJoiner("");
        List<String> groups = new LinkedList<>();

        for (String input : inputs) {
            if(input.trim().equals("")) {
                groups.add(joiner.toString());
                joiner = new StringJoiner("");
            } else {
                joiner.add(input);
            }
        }
        groups.add(joiner.toString());
//        System.out.println("groups = " + groups);

        int sum = 0;
        for (String group : groups) {
            List<Character> characterList = Chars.asList(group.toCharArray());
            Set<Character> characterSet = new HashSet<>(characterList);
//            System.out.println("characterSet = " + characterSet);
            sum += characterSet.size();
        }

        System.out.println("sum = " + sum);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day06.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
