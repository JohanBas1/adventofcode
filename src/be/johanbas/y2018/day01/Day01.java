package be.johanbas.y2018.day01;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Day01 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        part1();
        part2();

    }

    private static void part2() throws URISyntaxException, IOException {
        int sum = 0;
        Set<Integer> frequencies = new HashSet<>();
        List<String> strings = readInput();

        boolean loop = true;
        while (loop) {
//            System.out.println("new loop");

            for (String string : strings) {

                if (string.charAt(0) == '+') {
                    sum += Integer.parseInt(string.substring(1));
                } else {
                    sum -= Integer.parseInt(string.substring(1));
                }

                if (frequencies.contains(sum)) {
                    System.out.println("duplicate found = " + sum);
                    loop = false;
                    break;
                } else {
                    frequencies.add(sum);
                }
            }
        }
    }

    private static void part1() throws URISyntaxException, IOException {
        int sum = 0;

        List<String> strings = readInput();
        for (String string : strings) {

            if(string.charAt(0) == '+') {
                sum += Integer.parseInt(string.substring(1));
            } else {
                sum -= Integer.parseInt(string.substring(1));
            }
        }

        System.out.println("sum = " + sum);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day01.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }

}
