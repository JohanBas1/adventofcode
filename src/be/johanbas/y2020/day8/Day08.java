package be.johanbas.y2020.day8;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day08 {

    public static void main(String[] args) throws IOException, URISyntaxException {

        var visited = new TreeSet<Integer>();
        var position = 0;
        var accumulator = 0;

        var input = readInput();

//        part1(visited, position, accumulator, input);

        var changeCounter = 0;

        while (true) {
            System.out.println("changeCounter = " + changeCounter);
            if(testChange(visited, position, input, accumulator, changeCounter)) {

                break;
            }

            visited = new TreeSet<Integer>();
            position = 0;
            accumulator = 0;
            changeCounter++;
        }

        System.out.println("changeCounter = " + changeCounter);

    }

    private static boolean testChange(TreeSet<Integer> visited, int position, List<String> input, int accumulator, int changeCounter) {
        int counter = 0;

        while(true) {
            if(visited.contains(position)) {
                return false;
            }

            if(position == input.size()) {
                return true;
            }

            if(position > input.size()) {
                return false;
            }

            visited.add(position);

            Matcher matcher = Pattern.compile("^(\\w{3}) (.)(\\d+)").matcher(input.get(position));
            matcher.matches();
            var instruction = matcher.group(1);
            var operation = matcher.group(2);
            var value = Integer.parseInt(matcher.group(3));

            if(instruction.equals("nop") || instruction.equals("jmp")) {
                if(counter == changeCounter) {
                    if(instruction.equals("nop")) {
                        instruction = "jmp";
                    } else {
                        instruction = "nop";
                    }
                }
                counter++;
            }

            switch (instruction) {
                case "acc":
                    if (operation.equals("+")) {
                        accumulator += value;
                    } else {
                        accumulator -= value;
                    }
                    position++;
                    break;

                case "jmp":
                    if (operation.equals("+")) {
                        position += value;
                    } else {
                        position -= value;
                    }
                    break;

                case "nop":
                    position++;
                    break;
            }
            System.out.println("accumulator = " + accumulator);
        }

    }

    private static void part1(TreeSet<Integer> visited, int position, int accumulator, List<String> input) {
        while (!visited.contains(position)) {
            visited.add(position);

            Matcher matcher = Pattern.compile("^(\\w{3}) (.)(\\d+)").matcher(input.get(position));
            matcher.matches();
            var instruction = matcher.group(1);
            var operation = matcher.group(2);
            var value = Integer.parseInt(matcher.group(3));

            switch (instruction) {
                case "acc":
                    if (operation.equals("+")) {
                        accumulator += value;
                    } else {
                        accumulator -= value;
                    }
                    position++;
                    break;

                case "jmp":
                    if (operation.equals("+")) {
                        position += value;
                    } else {
                        position -= value;
                    }
                    break;

                case "nop":
                    position++;
                    break;
            }
        }

        System.out.println("accumulator = " + accumulator);
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day08.class.getResource("input.txt").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
