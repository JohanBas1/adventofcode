package be.johanbas.y2015.day07;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/*

--- Day 7: Some Assembly Required ---

This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates!
Unfortunately, little Bobby is a little under the recommended age range, and he needs help assembling the circuit.

Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535).
A signal is provided to each wire by a gate, another wire, or some specific value.
Each wire can only get a signal from one source, but can provide its signal to multiple destinations. A gate provides no signal until all of its inputs have a signal.

The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y to an AND gate, and then connect its output to wire z.

For example:

123 -> x means that the signal 123 is provided to wire x.
x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.
Other possible gates include OR (bitwise OR) and RSHIFT (right-shift).
If, for some reason, you'd like to emulate the circuit instead, almost all programming languages (for example, C, JavaScript, or Python) provide operators for these gates.

For example, here is a simple circuit:

123 -> x
456 -> y
x AND y -> d
x OR y -> e
x LSHIFT 2 -> f
y RSHIFT 2 -> g
NOT x -> h
NOT y -> i
After it is run, these are the signals on the wires:

d: 72
e: 507
f: 492
g: 114
h: 65412
i: 65079
x: 123
y: 456
In little Bobby's kit's instructions booklet (provided as your puzzle input), what signal is ultimately provided to wire a?


--- Part Two ---

Now, take the signal you got on wire a, override wire b to that signal, and reset the other wires (including wire a). What new signal is ultimately provided to wire a?

 */
public class Day7 {

    private static Map<String,Integer> wires = new HashMap<>();

    public static void main(String[] args) throws IOException, URISyntaxException {



        List<String> input = readInput();
//        input = new LinkedList<>();
//        input.add("123 -> x");
//        input.add("456 -> y");
//        input.add("x AND y -> d");
//        input.add("x OR y -> e");
//        input.add("x LSHIFT 2 -> f");
//        input.add("y RSHIFT 2 -> g");
//        input.add("NOT x -> h");
//        input.add("NOT y -> i");

        input = readInput2();

        Integer prevA = null;
        while(!wires.containsKey("a") && wires.get("a") == prevA) {
            processInput(input);
            prevA =  wires.get("a");
        }

//        for (Map.Entry<String, Integer> stringIntegerEntry : wires.entrySet()) {
//            System.out.println(stringIntegerEntry.getKey() + " - " + stringIntegerEntry.getValue());
//        }
        System.out.println(wires.get("a"));
    }

    private static void processInput(List<String> input) {
        for (String s : input) {
//            System.out.println(s);

            String[] split = s.split(" ");
            if(split.length == 3 && !wires.containsKey(split[2]) && containsOrInt(split[0]) != null) { // niet elke keer overschrijven?
                wires.put(split[2],containsOrInt(split[0]));

            } else if(split.length == 4 && containsOrInt(split[1]) != null) { // NOT y -> i
                wires.put(split[3], (~containsOrInt(split[1])) & 0xFFFF);

            } else if(split.length == 5) {
                if(split[1].equals("AND") && containsOrInt(split[0]) != null && containsOrInt(split[2]) != null) { // x AND y -> d
                    wires.put(split[4], containsOrInt(split[0]) & containsOrInt(split[2]));
                } else if(split[1].equals("OR") && containsOrInt(split[0]) != null && containsOrInt(split[2]) != null) { // x OR y -> d
                    wires.put(split[4], containsOrInt(split[0]) | containsOrInt(split[2]));
                } else if(split[1].equals("LSHIFT") && containsOrInt(split[0]) != null && containsOrInt(split[2]) != null) { // x LSHIFT 2 -> f
                    wires.put(split[4], containsOrInt(split[0]) << containsOrInt(split[2]));
                } else if(split[1].equals("RSHIFT") && containsOrInt(split[0]) != null && containsOrInt(split[2]) != null) { // lf RSHIFT 2 -> lg
                    wires.put(split[4], containsOrInt(split[0]) >> containsOrInt(split[2]));
                }
            }
        }
    }

    private static Integer containsOrInt(String input) {
        if(wires.containsKey(input)) {
            return wires.get(input);
        } else {
            try {
                int i = Integer.parseInt(input);
                return i;
            } catch (Exception e) {
                return null;
            }
        }
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        URI input = Day7.class.getResource("input").toURI();
        return Files.readAllLines(Paths.get(input));
    }

    private static List<String> readInput2() throws URISyntaxException, IOException {
        URI input = Day7.class.getResource("input2").toURI();
        return Files.readAllLines(Paths.get(input));
    }
}
