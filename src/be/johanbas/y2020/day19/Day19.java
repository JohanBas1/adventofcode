package be.johanbas.y2020.day19;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day19 {

    public static void main(String[] args) throws URISyntaxException, IOException {
        var input = readInput();

        var allRules = new HashMap<String, Rule>();
        input.stream()
                .map(Day19::createRule)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .forEach(rule -> allRules.put(rule.getRuleNumber(), rule));

        var rule = getFormattedRule(allRules, "0");
//        System.out.println("rule = " + rule);

        var messages = input.stream()
                .filter(s -> !s.contains(":"))
                .filter(s -> !s.isBlank())
                .toList();

        var part1 = messages.stream().filter(s -> s.matches(rule)).count();
        System.out.println("part1 = " + part1);

        ///////////////////////////////////// Part 2 //////////////////////////////////////////////////
        var r42 = allRules.get("42").computeRule(allRules);
        var r31 = allRules.get("31").computeRule(allRules);

        var r8 = String.format("(%s)+", r42); // 8: 42 | 42 8 👉 1 of meer keer regel 42 = (42)+

        // 11: 42 31 | 42 11 31
        // 👉 x keer 42, x keer 31 (x moet hetzelfde zijn ...) (r42(r42(r42r31)r31)r31)
        // 👉 maak 10 levels
        List<String> r11Options = new ArrayList<>();
        r11Options.add(String.format("(%s%s)",r42, r31));
        for (var i=0; i<10; i++) {
            r11Options.add(String.format("(%s%s%s)", r42, r11Options.get(i), r31));
        }

        var r0 = String.format("^(%s)$",r11Options.stream().map(o -> String.format("(%s%s)", r8, o)).collect(Collectors.joining("|")));
//        System.out.println("r0 = " + r0);

        var part2 = messages.stream().filter(s -> s.matches(r0)).count();
        System.out.println("part2 = " + part2);
    }

    private static String getFormattedRule(HashMap<String, Rule> allRules, String id) {
        return String.format("^(%s)$", allRules.get(id).computeRule(allRules));
    }

    private static List<String> readInput() throws URISyntaxException, IOException {
        var input = Objects.requireNonNull(Day19.class.getResource("input.txt")).toURI();
        return Files.readAllLines(Paths.get(input));
    }

    public static Optional<Rule> createRule(String s) {
        var pattern = Pattern.compile("(?<RuleNumber>(\\d+)): (?<RuleValue>(.*))");
        var matcher = pattern.matcher(s);

        if (matcher.matches()) {
            return Optional.of(new Rule(matcher.group("RuleNumber"), matcher.group("RuleValue")));
        }

        return Optional.empty();
    }
}

