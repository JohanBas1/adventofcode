package be.johanbas.y2015.day21;

/**
 * Created by Johan Bas.
 */
public class Character {

    private int hitpoints;
    private int damage;
    private int armor;

    public Character(int hitpoints, int damage, int armor) {
        this.hitpoints = hitpoints;
        this.damage = damage;
        this.armor = armor;
    }

    public boolean alive() {
        return hitpoints > 0;
    }

    public void attack(Character character) {
        character.defend(damage);
    }

    private void defend(int damage) {
        if(damage > armor) {
            hitpoints = hitpoints - damage + armor;
        } else {
            hitpoints--;
        }
    }
}
