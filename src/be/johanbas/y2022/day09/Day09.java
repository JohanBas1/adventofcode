package be.johanbas.y2022.day09;

import be.johanbas.tools.Filez;

import java.util.*;

public class Day09 {

    private static Map<Integer,Set<Pair>> tailPositions = new HashMap<>();

    private static List<Pair> rope = new LinkedList<>();

    public static void main(String[] args) {

        rope.add(new Pair(0,0)); // head

        for(var i=0; i<9; i++) { // tails
            rope.add(new Pair(0,0));
            var list = new LinkedHashSet<Pair>();
            list.add(new Pair(0,0));
            tailPositions.put(i+1, list);
        }

        var input = Filez.readFile("example.txt", Day09.class).stream().map(s -> s.split(" ")).map(s -> new Move(s[0], Integer.parseInt(s[1]))).toList();

        var counter = 0;
        for (var move : input) {
            moveHead(move);
            for (var z = 1; z < rope.size(); z++) {
                moveTailCloser(z);
            }
            counter++;
            System.out.println("move = " + counter);
        }

        System.out.println("part1: " + tailPositions.get(1).size());

        tailPositions.forEach((key, value) -> System.out.println(key + " " + value.size())); // 2222 to low
    }

    private static void moveHead(Move move) {
        var head = rope.get(0);
        if (move.direction().equals("R")) {
            head.setX(rope.get(0).getX() + move.steps());
            head.setY(rope.get(0).getY());
        } else if (move.direction().equals("D")) {
             head.setX(rope.get(0).getX());
             head.setY(rope.get(0).getY() + move.steps());
        } else if (move.direction().equals("L")) {
             head.setX(rope.get(0).getX() - move.steps());
             head.setY(rope.get(0).getY());
        } else if (move.direction().equals("U")) {
             head.setX(rope.get(0).getX());
             head.setY(rope.get(0).getY() - move.steps());
        }
    }

    private static void moveTailCloser(int i) {
        var head = rope.get(i-1);

        var newY = rope.get(i).getY();
        var newX = rope.get(i).getX();

        while(Math.abs(newY - head.getY()) > 1 || Math.abs(newX - head.getX()) > 1) {
            // move tail closer
            if(Math.abs(newY - head.getY()) > 0) {
                newY = newY - head.getY() < 0 ? newY+1 : newY-1;
            }
            if(Math.abs(newX - head.getX()) > 0) {
                newX = newX - head.getX() < 0 ? newX+1 : newX-1;
            }
            rope.get(i).setY(newY);
            rope.get(i).setX(newX);

            tailPositions.get(i).add(new Pair(newX, newY));
        }
    }
}

final class Pair {
    private int x;
    private int y;

    Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        var that = (Pair) obj;
        return this.x == that.x &&
                this.y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "Pair[" +
                "x=" + x + ", " +
                "y=" + y + ']';
    }
}

record Move(String direction, int steps) {}