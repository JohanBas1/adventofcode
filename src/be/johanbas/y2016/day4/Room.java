package be.johanbas.day4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Johan Bas on 6/12/2016.
 */
public class Room {

	private final int id;
	private final String checksum;
	private final String[] values;

	public Room(String roomString) {
		values = roomString.split("-");
		id = Integer.parseInt(values[values.length-1].split("\\[")[0]);
		checksum = values[values.length-1].split("\\[")[1].replace("]","");

		String temp = roomString.split("\\[")[0];
	}

	public int getId() {
		return id;
	}

	public boolean isCorrectRoom() {
		Map<String,Integer> count = new HashMap<>();

		for (int i = 0; i < values.length-1; i++) {
			for (int j = 0; j < values[i].length(); j++){

				String letter = values[i].charAt(j)+"";
				int x = count.getOrDefault(letter,0);

				count.put(letter, x+1);
			}
		}

		final List<String> temp = count.entrySet()
				.stream()
				.sorted((o1, o2) -> {
					if (o2.getValue().equals(o1.getValue())) {
						return o1.getKey().compareTo(o2.getKey());
					} else {
						return o2.getValue().compareTo(o1.getValue());
					}
				})
				.limit(5).map(Map.Entry::getKey).collect(Collectors.toList());

		String key = "";
		for (String s : temp) {
			key += s;
		}

		return key.equals(checksum);
	}

	public void decrypt() {
		for (int i = 0; i < values.length-1; i++) {
			for (int j = 0; j < values[i].length(); j++){

				int letter = values[i].charAt(j) - 96;
				char t = (char) (((letter + id) % 26) + 96);
				System.out.print(t);
			}
			System.out.print(" ");
		}
		System.out.print(" - "+id);
		System.out.println("");
	}
}
