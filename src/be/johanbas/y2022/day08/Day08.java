package be.johanbas.y2022.day08;

import be.johanbas.tools.Filez;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Day08 {

    public static int visibilityCounter = 0;

    public static void main(String[] args) {

        var input = Filez.readFile("input.txt", Day08.class);
        var grid = input.stream().map(String::toCharArray).map(Day08::charArrayToIntList).toList();

        var scenicScores = new LinkedList<Integer>();

        for (var row = 0; row < grid.size(); row++) {
            for (var column = 0; column < grid.get(row).size(); column++) {

                part1(grid, row, column);

                scenicScores.add(calculateScenicScore(grid, row, column));
            }
        }

        System.out.println("part1 = " + visibilityCounter);
        var part2 = scenicScores.stream().reduce(Integer::max).orElse(-1);
        System.out.println("part2 = " + part2);


    }

    private static int calculateScenicScore(List<List<Integer>> grid, int row, int column) {
        return scenicBottom(grid, row, column) * scenicTop(grid, row, column) * scenicLeft(grid, row, column) * scenicRight(grid, row, column);
    }

    private static void part1(List<List<Integer>> grid, int row, int column) {
        if (isVisibleFromTop(grid, row, column) || isVisibleFromBottom(grid, row, column) ||
                isVisibleFromRight(grid, row, column) || isVisibleFromLeft(grid, row, column)) {
            visibilityCounter++;
        }
    }

    private static int scenicBottom(List<List<Integer>> grid, int row, int column) {
        var scenicScore = 0;
        for (var x = row + 1; x < grid.size(); x++) {
            if (grid.get(x).get(column) >= grid.get(row).get(column)) {
                return scenicScore+1;
            }
            scenicScore++;
        }

        return scenicScore;
    }

    private static boolean isVisibleFromBottom(List<List<Integer>> grid, int row, int column) {
        var treesToCheck = new LinkedList<Integer>();
        for (var x = row + 1; x < grid.size(); x++) {
            treesToCheck.add(grid.get(x).get(column));
        }
        return treesToCheck.stream().reduce(Integer::max).orElse(-1) < grid.get(row).get(column);
    }

    private static int scenicTop(List<List<Integer>> grid, int row, int column) {
        var scenicScore = 0;
        for (var x = row-1; x >= 0; x--) {
            if (grid.get(x).get(column) >= grid.get(row).get(column)) {
                return scenicScore+1;
            }
            scenicScore++;
        }

        return scenicScore;
    }

    private static boolean isVisibleFromTop(List<List<Integer>> grid, int row, int column) {
        var treesToCheck = new LinkedList<Integer>();
        for (var x = 0; x < row; x++) {
            treesToCheck.add(grid.get(x).get(column));
        }
        return treesToCheck.stream().reduce(Integer::max).orElse(-1) < grid.get(row).get(column);
    }

    private static int scenicLeft(List<List<Integer>> grid, int row, int column) {
        var scenicScore = 0;
        for (var x = column-1; x >= 0; x--) {
            if (grid.get(row).get(x) >= grid.get(row).get(column)) {
                return scenicScore+1;
            }
            scenicScore++;
        }

        return scenicScore;
    }

    private static boolean isVisibleFromLeft(List<List<Integer>> grid, int row, int column) {
        var treesToCheck = new LinkedList<Integer>();
        for (var x = 0; x < column; x++) {
            treesToCheck.add(grid.get(row).get(x));
        }
        return treesToCheck.stream().reduce(Integer::max).orElse(-1) < grid.get(row).get(column);
    }

    private static int scenicRight(List<List<Integer>> grid, int row, int column) {
        var scenicScore = 0;
        for (var x = column + 1; x < grid.get(0).size(); x++) {
            if (grid.get(row).get(x) >= grid.get(row).get(column)) {
                return scenicScore+1;
            }
            scenicScore++;
        }

        return scenicScore;
    }

    private static boolean isVisibleFromRight(List<List<Integer>> grid, int row, int column) {
        var treesToCheck = new LinkedList<Integer>();
        for (var x = column + 1; x < grid.get(0).size(); x++) {
            treesToCheck.add(grid.get(row).get(x));
        }
        return treesToCheck.stream().reduce(Integer::max).orElse(-1) < grid.get(row).get(column);
    }

    private static List<Integer> charArrayToIntList(char[] s) {
        return IntStream.range(0, s.length).mapToObj(i -> s[i]).map(x -> Integer.parseInt(x + "")).toList();
    }
}
